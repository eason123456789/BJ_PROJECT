package bj.po;

import lombok.Data;

import java.io.Serializable;

/**
 * @author eason
 */
@Data
public class User implements Serializable {

    private String id;

    private String name;

    private String password;

    private String code;
}
