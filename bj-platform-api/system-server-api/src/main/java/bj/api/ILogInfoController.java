package bj.api;

import bj.core.ResultData;
import bj.po.LoginInfoPo;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;

@RequestMapping(value = "/restapi/logininfo")
public interface ILogInfoController {

    @PostMapping
    public ResultData addLoginInfo(@RequestBody LoginInfoPo loginInfoPo);
}
