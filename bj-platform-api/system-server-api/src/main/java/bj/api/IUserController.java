package bj.api;

import bj.core.ResultData;
import bj.po.LoginUser;
import bj.po.User;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

/**
 * @author eason
 */
@RequestMapping(value = "/restapi/user")
public interface IUserController {

    @GetMapping(value = "/getUserByName")
    public ResultData<LoginUser> selectUserByUserName(@RequestParam(value = "username",required = false) String userName);
}
