package bj.api;

import bj.core.ResultData;
import bj.po.SysLogPo;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * @author eason
 */
@RequestMapping("/restapi/log")
public interface ILogController {

    @PostMapping("/add")
    ResultData<Integer> saveLog(SysLogPo sysLogPo);
}
