package bj.observerPatternDemo;

/**
 * @ClassName ClientDemo
 * @Description 客户端测试类
 * @Author ctjsoft-pt
 * @Date 2/6/2023 上午 10:51
 * @Versin 1.0
 **/
public class ClientDemo {

    public static void main(String[] args) {

        //创建多个观察者
        ConcreteObserve observeA = new ConcreteObserve();
        observeA.setName("毛球");
        ConcreteObserve observeB = new ConcreteObserve();
        observeB.setName("二秃子");
        ConcreteObserve observeC = new ConcreteObserve();
        observeC.setName("小红");

        //创建 被观察者
        ConcreteSubject subject = new ConcreteSubject();
        //新增订阅
        subject.attach(observeA);
        subject.attach(observeB);
        subject.attach(observeC);
        //被观察者 发布消息
        subject.setContent("2023年6月2日 11:05:33");
    }

}
