package bj.observerPatternDemo;

/**
 * @ClassName Observer
 * @Description 观察者对象
 * @Author ctjsoft-pt
 * @Date 2/6/2023 上午 10:22
 * @Versin 1.0
 **/
public interface Observer {

    /**
     * 更新的接口 被通知的方法
     *
     * @param subject 传入目标对象,方便获取相应的目标对象状态 具体的目标对象  可以获取报纸的内容
     */
    public void update(Subject subject);
}
