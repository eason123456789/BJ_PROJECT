package bj.observerPatternDemo;

/**
 * @ClassName ConcreteSubject
 * @Description <p>具体的目标对象 负责把有关的状态存入到相应的观察者对象</p>
 * <p>并在自己状态转变时,通知各个观察者</p>
 * @Author ctjsoft-pt
 * @Date 2/6/2023 上午 10:31
 * @Versin 1.0
 **/
public class ConcreteSubject extends Subject {

    /**
     * 示意,目标对象的状态  报纸的具体内容
     */
    private String content;

    /**
     * 获取报纸具体内容
     *
     * @return 报纸具体内容
     */
    public String getContent() {
        return this.content;
    }

    /**
     * 设置报纸具体内容 相当于要出版报纸
     *
     * @param content 报纸具体内容
     */
    public void setContent(String content) {
        this.content = content;
        //状态发生改变  通知各个观察者 说明报纸又出版了 通知所有读者
        this.notifyObservers();
    }
}
