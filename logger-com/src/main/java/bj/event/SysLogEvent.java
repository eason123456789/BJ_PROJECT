package bj.event;

import bj.po.SysLogPo;

/**
 * @author eason
 */
public class SysLogEvent {

    private final SysLogPo sysLogPo;

    public SysLogEvent(SysLogPo sysLogPo) {
        this.sysLogPo = sysLogPo;
    }

    public SysLogPo getSysLogPo() {
        return sysLogPo;
    }
}
