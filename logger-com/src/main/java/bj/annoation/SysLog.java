package bj.annoation;

import java.lang.annotation.*;

/**
 * @author eason
 */
@Target(ElementType.METHOD)
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface SysLog {

    String value();
}
