package bj.aspect;

import bj.annoation.SysLog;
import bj.event.SysLogEvent;
import bj.po.SysLogPo;
import bj.util.LogTypeEnum;
import bj.util.SysLogUtil;
import lombok.RequiredArgsConstructor;
import lombok.SneakyThrows;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.ApplicationEventPublisher;

import java.util.Map;

/**
 * @author eason
 */
@Aspect
@RequiredArgsConstructor
public class SysLogAspect {

    private static final Logger log = LoggerFactory.getLogger(SysLogAspect.class);


    private final ApplicationEventPublisher publisher;


    @Value("${spring.application.name:}")
    private String serverName;


    @SneakyThrows
    @Around("@annotation(sysLog)")
    @SysLog("")
    public Object around(ProceedingJoinPoint point, SysLog sysLog) {

        String strClassName = point.getTarget().getClass().getName();
        String strMethodName = point.getSignature().getName();
        log.debug("[类名]:{},[方法]:{}", strClassName, strMethodName);

        SysLogPo sysLogPo = SysLogUtil.getSysLog(serverName,sysLog.value());

        // 发送异步日志事件
        Long startTime = System.currentTimeMillis();

        Object obj;
        try {
            obj = point.proceed() ;
        }
        catch (Exception e) {
            sysLogPo.setType(LogTypeEnum.ERROR.getType());
            sysLogPo.setException(e.getMessage());
            throw e;
        }finally {
            long endTime = System.currentTimeMillis();
            sysLogPo.setEraTime(endTime-startTime);
            publisher.publishEvent(new SysLogEvent(sysLogPo));
        }
        return obj;
    }



}
