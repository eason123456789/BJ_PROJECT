package bj.feign;

import bj.api.ILogInfoController;
import org.springframework.cloud.openfeign.FeignClient;

/**
 * @author eason
 */
@FeignClient(name = "system-server")
public interface LoginInfoFeign extends ILogInfoController {
}
