package bj.feign;

import bj.api.IUserController;
import org.springframework.cloud.openfeign.FeignClient;

/**
 * @author eason
 */
@FeignClient(name = "system-server")
public interface UserFeign extends IUserController {
}
