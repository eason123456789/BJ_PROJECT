package day0310;

import java.util.Scanner;

/**
 * @author eason
 */
public class HJ86 {

    /**
     * @Description:求最大连续的二进制数
     * @Author: Eason
     * @Date: 2023/3/17 下午1:57
     * @param: args
     * @return: void
     **/
    public static void main(String[] args) {
        Scanner input=new Scanner(System.in);
        int nextInt = input.nextInt();
        String s = Integer.toBinaryString(nextInt);
        String[] split = s.split("0");
        int max=0;
        for (int i = 0; i <split.length; i++) {
            if (split[i].length()>max){
                max=split[i].length();
            }
        }
        System.out.println(max);
    }
}
