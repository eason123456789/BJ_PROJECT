package day0310;

import java.util.Scanner;

/**
 * @Description:寻找最长子回文字符串
 * @Author: Eason
 * @Date: 2023/3/12 下午9:01
 * @param: null
 * @return: null
 **/
public class HJ85 {

    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        String s = input.nextLine();
        int max = 0;
        /**
         *双指针遍历找到最长子串
         */
        for (int i = 0; i <s.length()-1 ; i++) {
            for (int j = i+1; j <=s.length() ; j++) {
                String s1= s.substring(i,j);
                if (isPalindromeString(s1)){
                    max=max>j-i?max:j-i;
                }
            }
        }
        System.out.print(max);
    }

    /**
     *判断一个字符串是否是回文字符串的方法
     */
    static boolean isPalindromeString(String s) {
        return s.equals(new StringBuilder(s).reverse().toString());
    }
}
