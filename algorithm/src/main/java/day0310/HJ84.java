package day0310;

import java.util.Scanner;

/**
 * @author eason
 */
/**
统计大写字母个数
 **/
public class HJ84 {
    public static void main(String[] args) {
        Scanner in=new Scanner(System.in);
        String s = in.nextLine();
        String s1 = s.replaceAll("[^A-Z]", "");
        System.out.println(s1.length());
    }
}
