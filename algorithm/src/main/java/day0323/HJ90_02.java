package day0323;

/**
 * @author eason
 */

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

/**
 小易有一个长度为n的整数序列,a_1,...,a_n。然后考虑在一个空序列b上进行n次以下操作:
 1、将a_i放入b序列的末尾
 2、逆置b序列
 小易需要你计算输出操作n次之后的b序列。
 输入描述：
 输入包括两行,第一行包括一个整数n(2 ≤ n ≤ 2*10^5),即序列的长度。 第二行包括n个整数a_i(1 ≤ a_i ≤ 10^9),即序列a中的每个整数,以空格分割。
 输出描述：
 在一行中输出操作n次之后的b序列,以空格分割,行末无空格。
 示例1
 输入：
 4
 1 2 3 4
 输出：
 4 2 1 3
 **/
public class HJ90_02 {

    public static void main(String[] args) {
        Scanner scanner=new Scanner(System.in);
        int nextInt = scanner.nextInt();
        StringBuilder stringBuilder=new StringBuilder(128);
        for (int i = 0; i <nextInt ; i++) {
            String next = scanner.next();
            if (i==nextInt-1){
                stringBuilder.append(next);
            }else {
                stringBuilder.append(next + " ");
            }
        }
        System.out.println(rersve(stringBuilder.toString(),nextInt));
    }
     public static String rersve(String str,int length){

         String[] s = str.split(" ");
         List<String> list=new ArrayList<>();
         for (int i = 0; i <length ; i++) {
             if (i==0 || i%2==1){
                 list.add(0,s[i]);
             }else {
                 list.add(i,s[i]);
             }
         }
         StringBuilder stringBuilder=new StringBuilder(128);
         for (int i = 0; i <list.size() ; i++) {
             if (i==list.size()-1){
                 stringBuilder.append(list.get(i));
             }else {
                 stringBuilder.append(list.get(i) + " ");
             }
         }
         if (length%2!=0) {
             return stringBuilder.reverse().toString();
         }else {
             return stringBuilder.toString();
         }
     }
}
