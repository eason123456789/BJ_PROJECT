package day0323;

/**
 * @author eason
 */


import java.nio.charset.Charset;
import java.util.Scanner;

/**
 IPV4地址可以用一个32位无符号整数来表示，一般用点分方式来显示，点将IP地址分成4个部分，
 每个部分为8位，表示成一个无符号整数（因此正号不需要出现），如10.137.17.1，是我们非常熟悉的IP地址，
 一个IP地址串中没有空格出现（因为要表示成一个32数字）。

 现在需要你用程序来判断IP是否合法。

 输入描述：
 输入一个ip地址，保证不包含空格

 输出描述：
 返回判断的结果YES or NO

 示例1
 输入：
 255.255.255.1000
 输出：
 NO
 **/
public class HJ90 {
    public static void main(String[] args) {
        Scanner scanner=new Scanner(System.in);
        String s = scanner.nextLine();
        String[] split = s.split("\\.");
        String result="YES";
        if (split.length==4) {
            for (String s1 : split) {
                if (s1.length()<=0 || s1.length()>3){
                    result="NO";
                    break;
                }
                if (Integer.parseInt(s1)>255 || Integer.parseInt(s1)<0){
                    result="NO";
                    break;
                }
                if (s1.charAt(0)=='0'&& s1.length()!=1){
                    result="NO";
                    break;
                }
                for (char aChar : s1.toCharArray()) {
                    if (!Character.isDigit(aChar)){
                        result="NO";
                        break;
                    }
                }
            }
        }else {
            result="NO";
        }

        System.out.println(result);
    }
}
