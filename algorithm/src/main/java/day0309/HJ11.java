package day0309;

import java.util.Scanner;
/**
 输入描述：
 输入一个int整数

 输出描述：
 将这个整数以字符串的形式逆序输出

 示例1
 输入：
 1516000
 输出：
 0006151
 **/
public class HJ11 {

    public static void main(String[] args) {
        Scanner sc=new Scanner(System.in);
        String s = sc.nextLine();
        StringBuffer stringBuffer=new StringBuffer(s);
        StringBuffer reverse = stringBuffer.reverse();
        System.out.println(reverse);
    }
}
