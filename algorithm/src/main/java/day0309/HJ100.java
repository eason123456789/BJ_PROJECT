package day0309;

import java.util.Scanner;

/**
 * @author eason
 */
/**
 * @Description:   等差数列前n项和
 * @Author: Eason
 * @Date: 2023/3/20 下午4:16
 * @param: null
 * @return: null
 **/
public class HJ100 {

    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        int i = in.nextInt();
        int sn=(2*i)+(i*(i-1)*3)/2;
        System.out.println(sn);
    }
}
