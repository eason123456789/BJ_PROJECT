package day0309;

import java.util.Scanner;

/**
 * @author eason
 */
/**
 输入描述：
 输入一行，为一个只包含小写字母的字符串。

 输出描述：
 输出该字符串反转后的字符串。

 示例1
 输入：
 abcd
 输出：
 dcba
 **/
public class HJ12 {

    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        String s = in.nextLine();
        StringBuilder stringBuilder=new StringBuilder(s);
        StringBuilder reverse = stringBuilder.reverse();
        System.out.println(reverse);

    }
}
