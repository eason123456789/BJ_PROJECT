package day0309;

import java.util.Scanner;

/**
 * @author eason
 */
/**
 密码按如下规则进行计分，并根据不同的得分为密码进行安全等级划分。

 一、密码长度:
 5 分: 小于等于4 个字符
 10 分: 5 到7 字符
 25 分: 大于等于8 个字符

 二、字母:
 0 分: 没有字母
 10 分: 密码里的字母全都是小（大）写字母
 20 分: 密码里的字母符合”大小写混合“

 三、数字:
 0 分: 没有数字
 10 分: 1 个数字
 20 分: 大于1 个数字

 四、符号:
 0 分: 没有符号
 10 分: 1 个符号
 25 分: 大于1 个符号

 五、奖励（只能选符合最多的那一种奖励）:
 2 分: 字母和数字
 3 分: 字母、数字和符号
 5 分: 大小写字母、数字和符号

 最后的评分标准:
 >= 90: 非常安全
 >= 80: 安全（Secure）
 >= 70: 非常强
 >= 60: 强（Strong）
 >= 50: 一般（Average）
 >= 25: 弱（Weak）
 >= 0:  非常弱（Very_Weak）

 对应输出为：

 VERY_SECURE
 SECURE
 VERY_STRONG
 STRONG
 AVERAGE
 WEAK
 VERY_WEAK
 **/
public class HJ87 {

    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        String s = in.nextLine();
        int length = s.length();
        int score=0;
         if (length<=4){
             score+=5;
         }else if (length>=5 && length<=7){
             score+=10;
        }else {
             score+=25;
         }
        String s1 = s.replaceAll("[^A-Z]", "");
        String s2 = s.replaceAll("[^a-z]", "");
        if (s1.length()==0&&s2.length()==0){
            score+=0;
        }else if (s1.length()>0&&s2.length()>0){
            score+=20;
        }else {
            score+=10;
        }
        String s3 = s.replaceAll("[^0-9]", "");
        if (s3.length()>1){
            score+=20;
        }
        if (s3.length()==1){
            score+=10;
        }

        String s4 = s.replaceAll("[a-zA-Z0-9]", "");
        if (s4.length()>1){
            score+=25;
        }
        if (s4.length()==1){
            score+=10;
        }

        if ((s1.length()+s2.length()>0 && s3.length()>0) && s4.length()<=0)
        {
            score+=2;
        }
        if ((s1.length()+s2.length()>0 && s3.length()>0 && s4.length()>0) && (s1.length()==0 || s2.length()==0))
        {
            score+=3;
        }
        if (s1.length()>0 && s2.length()>0 && s3.length()>0 && s4.length()>0)
        {
            score+=5;
        }
        System.out.println(score);
        if (score>=90) System.out.println("VERY_SECURE");
        else if (score>=80) System.out.println("SECURE");
        else if (score>=70) System.out.println("VERY_STRONG");
        else if (score>=60) System.out.println("STRONG");
        else if (score>=50) System.out.println("AVERAGE");
        else if (score>=25) System.out.println("WEAK");
        else if (score>=0) System.out.println("VERY_WEAK");

    }
}
