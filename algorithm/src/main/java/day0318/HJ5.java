package day0318;

import java.util.Scanner;

/**
 * @author eason
 *
 * 写出一个程序，接受一个十六进制的数，输出该数值的十进制表示。
 */
public class HJ5 {
    public static void main(String[] args) {
        Scanner scanner=new Scanner(System.in);
        String nextInt = scanner.nextLine();
        String substring = nextInt.substring(2);
        Integer integer = Integer.parseUnsignedInt(substring,16);
        System.out.println(integer);
    }
}
