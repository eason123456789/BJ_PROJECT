package day0318;

import java.util.Scanner;

/**
 * @author eason
 *
 *
 * 找出字符串中第一个只出现一次的字符
 */
public class HJ59 {
    public static void main(String[] args) {
        Scanner scanner=new Scanner(System.in);
        String s = scanner.nextLine();
        boolean flag=true;
        for (int i = 0; i <s.length() ; i++) {
            char c = s.charAt(i);
            String s1 = s.replaceAll(c + "", "");
            if (s.length()-s1.length()==1){
                System.out.println(c);
                flag=false;
                break;
            }
        }
        if (flag){
            System.out.println(-1);
        }

    }
}
