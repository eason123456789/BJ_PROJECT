package day0318;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

/**
 * @author eason
 *
 * 描述
 * 任意一个偶数（大于2）都可以由2个素数组成，组成偶数的2个素数有很多种情况，本题目要求输出组成指定偶数的两个素数差值最小的素数对。
 *
 * 输入描述：
 * 输入一个大于2的偶数
 *
 * 输出描述：
 * 从小到大输出两个素数
 *
 * 示例1
 * 输入：
 * 20
 * 输出：
 * 7
 * 13
 */
public class HJ60 {
    public static void main(String[] args) {
        Scanner scanner=new Scanner(System.in);
        int nextInt = scanner.nextInt();
        observe(nextInt);
    }

    /**
     * @Description:找出差值最小的两个素数
     * @Author: Eason
     * @Date: 2023/3/18 下午5:50
     * @param: num
     * @return: void
     **/
    public static void observe(int num){
        int min=Integer.MAX_VALUE;
        List<Object> list=new ArrayList<>();
        for(int i = 2; i < num; i++) {
            if(isPrime(i) && isPrime(num - i)) {
                // 保存最接近的两个素数
                if(Math.abs(num - i - i) < min) {
                    list.add(0,i);
                    list.add(1,num-i);
                    min = Math.abs(num - i - i);
                }
            }
        }
        System.out.println(list.get(0)+"\n"+list.get(1));
    }

    /**
     * @Description: 判断是否为素数
     * @Author: Eason
     * @Date: 2023/3/18 下午5:36
     * @param: num
     * @return: boolean
     **/
    public static boolean isPrime(int num){
        for (int i = 2; i <=Math.sqrt(num) ; i++) {
            if (num%i==0){
                return false;
            }
        }
        return true;
    }
}
