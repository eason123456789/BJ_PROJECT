package day0318;

import java.util.Scanner;

/**
 * @author eason
 *
 *
 * 求int型正整数在内存中存储时1的个数
 */
public class HJ15 {
    public static void main(String[] args) {
        Scanner scanner=new Scanner(System.in);
        int nextInt = scanner.nextInt();
        String s = Integer.toBinaryString(nextInt);
        String s1 = s.replaceAll("1", "");
        System.out.println(s.length()-s1.length());
    }
}
