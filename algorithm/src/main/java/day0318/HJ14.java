package day0318;

import java.util.*;

/**
 * @author eason
 *
 *
 *
 * 描述
 * 给定 n 个字符串，请对 n 个字符串按照字典序排列。

 * 示例1
 * 输入：
 * 9
 * cap
 * to
 * cat
 * card
 * two
 * too
 * up
 * boat
 * boot
 * 输出：
 * boat
 * boot
 * cap
 * card
 * cat
 * to
 * too
 * two
 * up
 */
public class HJ14 {
    public static void main(String[] args) {
        Scanner scanner=new Scanner(System.in);
        int nextInt = scanner.nextInt();
        List<String> list=new ArrayList<>();
        for (int i = 0; i <nextInt ; i++) {
            String s = scanner.next();
            list.add(s);
        }
        Object[] objects = list.toArray();
        Arrays.sort(objects);
        for (Object s : objects) {
            System.out.println(s);
        }
    }
}
