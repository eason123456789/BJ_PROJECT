package day0317;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.Scanner;
import java.util.stream.Collectors;

/**
 * @author eason
 *
 *
 * 输入描述：
 * 第一行输入两个整数n和k
 * 第二行输入一个整数数组
 *
 * 输出描述：
 * 从小到大输出最小的k个整数，用空格分开。
 *
 * 示例1
 * 输入：
 * 5 2
 * 1 3 5 7 2
 * 输出：
 * 1 2
 *
 */
public class HJ58 {
    public static void main(String[] args) {
        Scanner scanner=new Scanner(System.in);
        int nextInt1 = scanner.nextInt();
        int nextInt2 = scanner.nextInt();
        List<Integer> list=new ArrayList<>();
        for (int i = 0; i <nextInt1; i++) {
            list.add(scanner.nextInt());
        }
        List<Integer> collect = list.stream().sorted().collect(Collectors.toList());
        for (int i = 0; i <nextInt2 ; i++) {
            System.out.print(collect.get(i)+" ");
        }

    }
}
