package day0317;

import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;

/**
 * @author eason
 */
/**
 输入描述：
 输入一行没有空格的字符串。

 输出描述：
 输出 输入字符串 中范围在(0~127，包括0和127)字符的种数。

 示例1
 输入：
 abc
 输出：
 3

 示例2
 输入：
 aaa
 输出：
 1
 **/
public class HJ10 {

    public static void main(String[] args) {
        Scanner scanner=new Scanner(System.in);
        String s = scanner.nextLine();
        char[] chars = s.toCharArray();
        Map<Character, String> map=new HashMap<>();
        for (char aChar : chars) {
            map.put(aChar,"111");
        }
        System.out.println(map.size());
    }
}
