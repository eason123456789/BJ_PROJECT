package day0317;

import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;

/**
 * @author eason
 *
 *
 * 描述
 * 判断短字符串S中的所有字符是否在长字符串T中全部出现。
 *
 * 输入描述：
 * 输入两个字符串。第一个为短字符串，第二个为长字符串。两个字符串均由小写字母组成。
 *
 * 输出描述：
 * 如果短字符串的所有字符均在长字符串中出现过，则输出字符串"true"。否则输出字符串"false"。
 *
 *
 * 输入：
 * bc
 * abc
 * 输出：
 * true
 * 说明：
 * 其中abc含有bc，输出"true"
 */
public class HJ81 {
    public static void main(String[] args) {
        Scanner scanner=new Scanner(System.in);
        String s2 = scanner.nextLine();
        String s1 = scanner.nextLine();
        char[] chars1= s1.toCharArray();
        char[] chars2= s2.toCharArray();
        Map<Character, String> map=new HashMap<>();
        boolean flag=true;
        for (char c : chars1) {
            map.put(c,"111");
        }
        for (char c : chars2) {
            if (map.get(c)==null){
                flag=false;
                break;
            }
        }
        System.out.println(flag);
    }
}
