package day0317;

import java.util.Scanner;

/**
 * @author eason
 */
public class HJ76 {
    /**
     * @Description:
     * @Author: Eason 
     * @Date: 2023/3/17 下午2:51
     * @param: args 
     * @return: void
     **/
    public static void main(String[] args) {
        Scanner scanner=new Scanner(System.in);
        int nextInt = scanner.nextInt();
        int start=nextInt*(nextInt-1)+1;
        StringBuilder s=new StringBuilder(128);
        for (int i = 0; i <nextInt ; i++) {
            if (i==nextInt-1){
                s.append(start);
            }else {
                s.append(start).append("+");
            }
            start=start+2;
        }
        System.out.println(s);
    }
}
