package day0227;

import java.util.Scanner;

/**
 * @author eason
 */
/**
 输入描述：
 输入一个正浮点数值
 输出描述：
 输出该数值的近似整数值
 **/
public class HJ7 {

    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        String s = in.nextLine();
        String[] split = s.split("\\.");
        String s2=split[1];
        String substring = s2.substring(0,1);
        int i = Integer.parseInt(substring);
        if (i>=5){
            System.out.println(Integer.parseInt(split[0])+1);
        }else {
            System.out.println(Integer.parseInt(split[0]));
        }

    }
}
