package day0227;

import java.util.Scanner;

/**
 * @author eason
 */
/**
 输入：
 hello nowcoder
 输出：
 8
 说明：
 最后一个单词为nowcoder，长度为8
 **/
public class HJ1 {

    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        String s = in.nextLine();
        String[] s1 = s.split(" ");
        System.out.println(s1[s1.length-1].length());
    }
}
