package day0227;

import java.util.Scanner;

/**
 * @author eason
 */
/**
输入描述：
第一行输入一个由字母、数字和空格组成的字符串，第二行输入一个字符（保证该字符不为空格）。
输出描述：
输出输入字符串中含有该字符的个数。（不区分大小写字母）
 输入：
 ABCabc
 A
 输出：
 2
 **/
public class HJ2 {
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        String s1 = in.nextLine();
        String s2 = in.nextLine();
        String[] split = s1.split("");
        int count=0;
        for (String s : split) {
            if(s2.equalsIgnoreCase(s)){
                count++;
            }
        }
        System.out.println(count);
    }
}
