package day0227;

import java.util.*;

/**
 * @author eason
 */
/**
 先输入键值对的个数n（1 <= n <= 500）
 接下来n行每行输入成对的index和value值，以空格隔开
 输出描述：
 输出合并后的键值对（多行）
 输入：
 4
 0 1
 0 2
 1 2
 3 4
 输出：
 0 3
 1 2
 3 4
 **/
public class HJ8 {

    public static void main(String[] args){
        Scanner sc = new Scanner(System.in);
        TreeMap<Integer,Integer> map = new TreeMap<>();
            int n = sc.nextInt();
            for(int i =0;i<n;i++){
                int key = sc.nextInt();
                int value = sc.nextInt();
                map.put(key,map.getOrDefault(key,0)+value);
            }
            for(Integer i : map.keySet()){
                System.out.println(i+" "+map.get(i));
            }
        }
}
