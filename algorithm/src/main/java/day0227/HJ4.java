package day0227;

import java.util.Scanner;

/**
 * @author eason
 */
/**
 输入描述：
 连续输入字符串(每个字符串长度小于等于100)

 输出描述：
 依次输出所有分割后的长度为8的新字符串
 输入：
 abc
 输出：
 abc00000
 **/
public class HJ4 {

    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        String s = in.nextLine();
        char[] chars = s.toCharArray();
        for (int i = 1; i <chars.length+1 ; i++) {
            System.out.print(chars[i-1]);
            if (i%8==0){
                System.out.println();
            }
        }
        if (chars.length%8!=0){
            for (int i = 0; i <8-chars.length%8; i++) {
                System.out.print('0');
            }
        }
    }
}
