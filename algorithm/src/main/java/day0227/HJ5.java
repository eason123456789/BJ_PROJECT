package day0227;

import java.util.Scanner;

/**
 * @author eason
 */
/**
 输入描述：
 输入一个十六进制的数值字符串。
 输出描述：
 输出该数值的十进制字符串。不同组的测试用例用\n隔开。
 输入：
 0xAA
 输出：
 170
 **/
public class HJ5 {

    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        String s = in.nextLine();
        int i = Integer.parseInt(s.substring(2), 16);
        System.out.println(i);
    }
}
