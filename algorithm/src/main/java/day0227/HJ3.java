package day0227;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;
import java.util.TreeSet;
import java.util.stream.Collectors;

/**
 * @author eason
 */
/**
 输入描述：
 第一行先输入随机整数的个数 N 。 接下来的 N 行每行输入一个整数，代表明明生成的随机数。 具体格式可以参考下面的"示例"。
 输出描述：
 输出多行，表示输入数据处理后的结果

 输入：
 3
 2
 2
 1
 输出：
 1
 2
 说明：
 输入解释：
 第一个数字是3，也即这个小样例的N=3，说明用计算机生成了3个1到500之间的随机整数，接下来每行一个随机数字，共3行，也即这3个随机数字为：
 2
 2
 1
 所以样例的输出为：
 1
 2
 **/
public class HJ3 {

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int i = scanner.nextInt();
        TreeSet<Object> list=new TreeSet<>();
        for (int j = 0; j < i; j++) {
            int i1 = scanner.nextInt();
            list.add(i1);
        }
        //List<Object> list1 = list.stream().sorted().distinct().collect(Collectors.toList());
        for (Object o : list) {
            System.out.println(o);
        }

    }
}
