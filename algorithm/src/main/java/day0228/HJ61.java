package day0228;

import java.io.IOException;
import java.util.Scanner;

/**
 * @author eason
 */
public class HJ61 {

    //动态规划&&递归
    public static void main(String[] args) throws IOException {
        Scanner in = new Scanner(System.in);
        while (in.hasNextInt()) {
            int apple = in.nextInt();
            int plate = in.nextInt();
            System.out.println(count(apple,plate));
        }
    }

    public static int count(int apple,int plate){
        //总共两大种情况
        //1. apple数量大于plate数量，可以分为空盘子或不空盘子（即每个盘子至少放一个）
        //2. apple数量小于plate数量，此时，至少有plate-apple个空盘（对于计算放盘的数量而言，空盘无效），只当做apple = plate来计算(这样会把计算交给第一类)
        //结束条件：apple == 0(第一大类的不空盘子可能的结果（苹果数量<=盘子数量时的一种可能性）);plate == 1(第一大类的不空盘子可能的结果/第一大类的空盘子可能的结果)
        if (apple==0 || plate==1){
            return 1;
        }else if (apple<plate){
            return count(apple,apple);
        }else {
            return count(apple-plate,plate)+count(apple,plate-1);
        }
    }
}
