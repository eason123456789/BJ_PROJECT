package day0327;

/**
 * @author eason
 */

/**
 描述
 在一个长度为n的数组里的所有数字都在0到n-1的范围内。 数组中某些数字是重复的，但不知道有几个数字是重复的。
 也不知道每个数字重复几次。请找出数组中任意一个重复的数字。 例如，如果输入长度为7的数组[2,3,1,0,2,5,3]，
 那么对应的输出是2或者3。存在不合法的输入的话输出-1

 示例1
 输入：
 [2,3,1,0,2,5,3]
 返回值：
 2
 说明：
 2或3都是对的
 **/
public class JZ3 {


    public static void main(String[] args) {
        int[] array=new int[]{2,3,1,0,2,5,3};
        int duplicate = duplicate(array);
        System.out.println(duplicate);
    }
    public static int duplicate (int[] nums) {
        for (int i = 0; i < nums.length; i++) {
            while (nums[i] != i) {
                if (nums[i] == nums[nums[i]]) {
                    return  nums[i];
                }
                swap(nums, i, nums[i]);
            }
        }
        return -1;
    }
    public static void swap(int[] numbers,int i,int j){
        int t=numbers[i];
        numbers[i]=numbers[j];
        numbers[j]=t;
    }
}
