package day0320;

/**
 * @author eason
 */
/**
 编写一个函数来查找字符串数组中的最长公共前缀。

 如果不存在公共前缀，返回空字符串 ""。

 示例 1：
 输入：strs = ["flower","flow","flight"]
 输出："fl"

 示例 2：
 输入：strs = ["dog","racecar","car"]
 输出：""
 解释：输入不存在公共前缀。
 **/
public class Leet14 {
    public static void main(String[] args) {
        String[] strs=new String[]{"flower","flow","flight"};
        String s = longestCommonPrefix(strs);
        System.out.println(s);
    }

    public static String longestCommonPrefix(String[] strs) {
        if (strs.length==0){
            return "";
        }
        String result=strs[0];

        for (int i=1; i <strs.length ; i++) {
            int j=0;
            for (; j < result.length() && j<strs[i].length(); j++) {
                if (result.charAt(j)!=strs[i].charAt(j)){
                    break;
                }
            }
            result=result.substring(0,j);
            if (result.equals("")){
                return result;
            }

        }
        return result;
    }
}
