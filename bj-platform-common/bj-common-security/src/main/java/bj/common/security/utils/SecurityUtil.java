package bj.common.security.utils;

import bj.common.security.context.SecurityContextHolder;
import bj.po.LoginUser;
import bj.utils.SecurityConstants;

/**
 * @author eason
 */
public class SecurityUtil {
    /**
     * 获取用户ID
     */
    public static String getUserId()
    {
        return SecurityContextHolder.getUserId();
    }

    /**
     * 获取用户名称
     */
    public static String getUsername()
    {
        return SecurityContextHolder.getUserName();
    }

    /**
     * 获取用户key
     */
    public static String getUserKey()
    {
        return SecurityContextHolder.getUserKey();
    }

    /**
     * 获取登录用户信息
     */
    public static LoginUser getLoginUser()
    {
        return SecurityContextHolder.get(SecurityConstants.LOGIN_USER, LoginUser.class);
    }
}
