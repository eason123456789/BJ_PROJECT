package bj.common.security.service;

import bj.po.LoginUser;
import bj.utils.RedisTemplateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * @author eason
 */
@Service(value = "TokenService")
public class TokenService {

    @Autowired
    private RedisTemplateUtils redisTemplate;

    public LoginUser getLoginUser(String tokenKey){
        LoginUser loginUser=redisTemplate.getCacheObject(tokenKey);
        return loginUser;
    }
}
