package bj.common.security.aspect;

import bj.common.security.annotation.RequiresPermissions;
import bj.common.security.context.SecurityContextHolder;
import bj.po.LoginUser;
import bj.utils.SecurityConstants;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.springframework.core.Ordered;
import org.springframework.stereotype.Component;

import java.util.Set;

/**
 * @author eason
 */
@Aspect
@Component
public class RequiresPermissionsAspect implements Ordered {

    @Around("@annotation(requiresPermissions)")
    public Object checkPermissions(ProceedingJoinPoint joinPoint, RequiresPermissions requiresPermissions) throws Throwable {
        String[] value = requiresPermissions.value();
        LoginUser loginUser = SecurityContextHolder.get(SecurityConstants.LOGIN_USER, LoginUser.class);
        if (loginUser==null){
            throw new RuntimeException("请先登录");

        }
        Set<String> permissions = loginUser.getPermissions();

        boolean contains = permissions.contains(value);

        if (contains)
        {
            throw new RuntimeException("未拥有该权限");
        }

        return joinPoint.proceed();
    }

    @Override
    public int getOrder() {
        return 1;
    }
}
