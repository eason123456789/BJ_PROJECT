package bj.common.security.annotation;

import java.lang.annotation.*;

/**
 * @author eason
 */
@Retention(RetentionPolicy.RUNTIME)
@Target({ ElementType.METHOD, ElementType.TYPE })
@Documented
public @interface RequiresPermissions {

    String[] value() default {};

    String logical() default "AND";
}
