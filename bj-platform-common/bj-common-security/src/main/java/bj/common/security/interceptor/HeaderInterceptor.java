package bj.common.security.interceptor;

import bj.common.security.context.SecurityContextHolder;
import bj.common.security.service.TokenService;
import bj.po.LoginUser;
import bj.utils.JwtUtil;
import bj.utils.RedisTemplateUtils;
import bj.utils.SecurityConstants;
import bj.utils.SpringUtils;
import jdk.nashorn.internal.objects.annotations.Constructor;
import org.apache.commons.codec.binary.Base64;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.ConfigurableListableBeanFactory;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.stereotype.Component;
import org.springframework.web.method.HandlerMethod;
import org.springframework.web.servlet.AsyncHandlerInterceptor;
import org.springframework.web.servlet.HandlerInterceptor;

import javax.annotation.PostConstruct;
import javax.annotation.Resource;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;


/**
 * @author eason
 *
 * 自定义请求头拦截器，将Header数据封装到线程变量中方便获取
 */
@Component
public class HeaderInterceptor implements AsyncHandlerInterceptor {

    public TokenService tokenService = SpringUtils.getBean(TokenService.class);

    @Override
    public boolean preHandle(javax.servlet.http.HttpServletRequest request, javax.servlet.http.HttpServletResponse response, Object handler)
            throws Exception {

        if (!(handler instanceof HandlerMethod))
        {
            return true;
        }

        String token = request.getHeader(JwtUtil.AUTHENTICATION);
        if (token!=null){
            String userKey = JwtUtil.getUserKey(token);
            String tokenKey = getTokenKey(userKey);
            LoginUser loginUser = tokenService.getLoginUser(tokenKey);
            if (loginUser!=null){
                SecurityContextHolder.set(SecurityConstants.LOGIN_USER,loginUser);
            }
            SecurityContextHolder.setUserId(new String(Base64.decodeBase64(request.getHeader(SecurityConstants.DETAILS_USER_ID).getBytes(StandardCharsets.UTF_8))));
            SecurityContextHolder.setUserName(new String(Base64.decodeBase64(request.getHeader(SecurityConstants.DETAILS_USERNAME).getBytes(StandardCharsets.UTF_8))));
            SecurityContextHolder.setUserKey(new String(Base64.decodeBase64(request.getHeader(SecurityConstants.USER_KEY).getBytes(StandardCharsets.UTF_8))));
        }
        return true;
    }

    @Override
    public void afterCompletion(javax.servlet.http.HttpServletRequest request, javax.servlet.http.HttpServletResponse response, Object handler, Exception ex)
            throws Exception {

        SecurityContextHolder.remove();

    }

    private String getTokenKey(String token) {
        return "login_token:" + token;
    }
}
