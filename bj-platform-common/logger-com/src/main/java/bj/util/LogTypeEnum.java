package bj.util;


import lombok.Data;
import lombok.Getter;
import lombok.RequiredArgsConstructor;

/**
 * @Description: 日志类型
 * @Author: GdlSky
 * @Date 下午4:30 2021/8/31
 */
@Getter
@RequiredArgsConstructor
public enum LogTypeEnum {

	/**
	 * 正常日志类型
	 */
	NORMAL("0", "正常日志"),

	/**
	 * 错误日志类型
	 */
	ERROR("9", "错误日志");

	/**
	 * 类型
	 */
	private final String type;

	/**
	 * 描述
	 */
	private final String description;

}