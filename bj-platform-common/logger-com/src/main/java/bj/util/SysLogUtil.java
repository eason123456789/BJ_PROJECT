package bj.util;

import bj.po.SysLogPo;
import cn.hutool.core.date.DateTime;
import cn.hutool.core.util.URLUtil;
import cn.hutool.extra.servlet.ServletUtil;
import lombok.experimental.UtilityClass;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import javax.servlet.http.HttpServletRequest;
import java.util.Date;
import java.util.Objects;
import java.util.UUID;

/**
 * @author eason
 */
@UtilityClass
public class SysLogUtil {

    public SysLogPo getSysLog(String serverName,String value) {
        HttpServletRequest request = ((ServletRequestAttributes) Objects
                .requireNonNull(RequestContextHolder.getRequestAttributes())).getRequest();
        SysLogPo sysLog = new SysLogPo();
        sysLog.setTitle(value);
        sysLog.setServiceId(serverName);
        sysLog.setId(UUID.randomUUID().toString().replaceAll("-",""));
        sysLog.setType(LogTypeEnum.NORMAL.getType());
        sysLog.setMethod(request.getMethod());
        sysLog.setRemoteAddr(ServletUtil.getClientIP(request).equals("0:0:0:0:0:0:0:1")?"127.0.0.1":ServletUtil.getClientIP(request));
        sysLog.setRequestUri(URLUtil.getPath(request.getRequestURI()));
        sysLog.setCreateTime(new Date());
        return sysLog;
    }
}
