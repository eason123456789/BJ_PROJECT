package bj.util;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.cloud.stream.messaging.Source;
import org.springframework.messaging.support.MessageBuilder;
import org.springframework.stereotype.Component;

import java.nio.charset.StandardCharsets;

@Component
@ConditionalOnProperty(prefix="ctj.exception.log",name="strategy",havingValue="rocketmq")
public class RocketmqStreamUtil {
    @Autowired
    private Source source;

    public void sendMessage(String body){
        this.source.output().send(MessageBuilder.withPayload(body.getBytes(StandardCharsets.UTF_8)).build());
    }
}
