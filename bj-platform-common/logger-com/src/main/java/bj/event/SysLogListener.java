package bj.event;

import bj.api.ILogController;
import bj.po.SysLogPo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.event.EventListener;
import org.springframework.core.annotation.Order;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Component;

/**
 * @author eason
 */
@Component
public class SysLogListener {

    @Autowired
    private ILogController logController;


    @Async
    @Order
    @EventListener(SysLogEvent.class)
    public void saveLog(SysLogEvent sysLogEvent){
        SysLogPo sysLogPo = sysLogEvent.getSysLogPo();
        logController.saveLog(sysLogPo);
    }
}
