package bj.po;



import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import org.springframework.format.annotation.DateTimeFormat;

import java.io.Serializable;
import java.util.Date;

/**
 * @author eason
 */
@Data
public class SysLogPo implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 编号
     */
    @ApiModelProperty(value = "id")
    private String id;
    /**
     * 日志类型
     */
    @ApiModelProperty(value = "请求结果0:成功9:失败")
//    @NotBlank(message = "日志类型不能为空")
    private String type;
    /**
     * 日志标题
     */
    @ApiModelProperty(value = "日志标题")
//    @NotBlank(message = "日志标题不能为空")
    private String title;
    /**
     * 应用标识
     */
    @ApiModelProperty(value = "应用标识")
    private String serviceId;
    /**
     * 操作人
     */
    @ApiModelProperty(value = "操作人")
    private String createBy;
    /**
     * 用户类型
     */
    @ApiModelProperty(value = "用户类型")
    private Long userType;
    /**
     * 用户代理
     */
    @ApiModelProperty(value = "用户代理")
    private String userAgent;
    /**
     * 创建时间
     */
    @JsonFormat(pattern = "yyyy-MM-dd", timezone = "GMT+8")
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    @ApiModelProperty(value = "创建时间", dataType = "LocalDate", example = "2020-12-07")
    private Date createTime;
    /**
     * 操作IP地址
     */
    @ApiModelProperty(value = "操作IP地址")
    private String remoteAddr;
    /**
     * 请求URI
     */
    @ApiModelProperty(value = "请求URI")
    private String requestUri;
    /**
     * 操作方式
     */
    @ApiModelProperty(value = "操作方式")
    private String method;
    /**
     * 提交数据
     */
    @ApiModelProperty(value = "提交数据")
    private String params;
    /**
     * 方法执行时间
     */
    @ApiModelProperty(value = "方法执行时间")
    private Long eraTime;
    /**
     * 删除标记
     */
    @ApiModelProperty(value = "删除标记：1、是 2、否")
    private Long delFlag;
    /**
     * 异常信息
     */
    @ApiModelProperty(value = "异常信息")
    private String exception;


}
