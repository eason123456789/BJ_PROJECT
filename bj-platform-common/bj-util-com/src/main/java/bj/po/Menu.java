package bj.po;

import lombok.Data;

import java.io.Serializable;

/**
 * @author eason
 */
@Data
public class Menu implements Serializable {
    private Long id;
    private String name;
    private String url;
    private Long parentId;
    private String permission;
}
