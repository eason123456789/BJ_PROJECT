package bj.po;

import com.yhq.sensitive.annotation.SensitiveInfo;
import com.yhq.sensitive.annotation.SensitivePassword;
import lombok.Data;

import java.io.Serializable;

/**
 * @author eason
 */
@Data
public class User implements Serializable {

    private String id;

    @SensitiveInfo(strategy = com.yhq.sensitive.strategy.SensitiveChineseName.class, begin = 1)
    private String name;
//    @SensitivePassword
    private String password;

    private String code;
}
