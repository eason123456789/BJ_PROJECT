package bj.po;

import lombok.Data;

import java.io.Serializable;

/**
 * @author eason
 */
@Data
public class Role implements Serializable {
    private Long id;
    private String name;
    private int is_admin;
}
