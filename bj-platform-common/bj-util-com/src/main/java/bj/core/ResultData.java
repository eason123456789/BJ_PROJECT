package bj.core;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

/**
 * @Description:返回值封装类
 * @Author: Eason
 * @Date: 2022/7/8 下午4:16
 **/
@Data
@AllArgsConstructor
@NoArgsConstructor
public class ResultData<T> implements Serializable {

    private static final long serialVersionUID = 1L;


    private String status_code = "0000";
    private String reason;
    private String code = "0000";
    private String message;
    private T data;
    private Long total;
    private Integer page;
    private Integer pageSize;
    private Long runtime;

    /**
     * 增删改返回值构造方法
     *
     * @param code
     * @param message
     */
    public ResultData(String code, String message) {
        this.code = code;
        this.message = message;
        this.status_code = code;
        this.reason = message;
    }

    /**
     * 批量增删改返回值构造方法
     *
     * @param code
     * @param message
     */
    public ResultData(String code, String message, long total) {
        this.code = code;
        this.message = message;
        this.total = total;
        this.status_code = code;
        this.reason = message;
    }

    /**
     * 单条和多条数据构造方法
     *
     * @param code
     * @param message
     * @param data
     */
    public ResultData(String code, String message, T data) {
        this.code = code;
        this.message = message;
        this.data = data;
        this.status_code = code;
        this.reason = message;
    }

    public ResultData(String code, String message, T data, long total) {
        this.code = code;
        this.message = message;
        this.data = data;
        this.total = total;
        this.status_code = code;
        this.reason = message;
    }

    /**
     * 分页查询构造方法
     *
     * @param code
     * @param message
     * @param data
     * @param total
     * @param page
     * @param pageSize
     */
    public ResultData(String code, String message, T data, long total, int page, int pageSize) {
        this.code = code;
        this.message = message;
        this.data = data;
        this.total = total;
        this.page = page;
        this.pageSize = pageSize;
        this.status_code = code;
        this.reason = message;
    }
}
