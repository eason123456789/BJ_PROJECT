//package bj.utils;
//
//import bj.po.LoginUser;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.stereotype.Component;
//import org.springframework.web.context.request.RequestContextHolder;
//import org.springframework.web.context.request.ServletRequestAttributes;
//
//import javax.servlet.http.HttpServletRequest;
//import java.util.Objects;
//
///**
// * @author eason
// */
//@Component
//public class SessionUtils {
//
//    @Autowired
//    private static RedisTemplateUtils redisTemplate;
//
//
//    public static LoginUser getSession() {
//
//        HttpServletRequest request = ((ServletRequestAttributes) Objects
//                .requireNonNull(RequestContextHolder.getRequestAttributes()))
//                .getRequest();
//        LoginUser loginuser = (LoginUser) request.getSession().getAttribute("loginuser");
//        if (loginuser==null){
//            loginuser= (LoginUser) getUser();
//        }
//        return loginuser;
//    }
//
//    public  static Object getUser(){
//        HttpServletRequest request = ((ServletRequestAttributes) Objects
//                .requireNonNull(RequestContextHolder.getRequestAttributes()))
//                .getRequest();
//        if (request == null) {
//            return null;
//        }
//        String token = request.getHeader(JwtUtil.AUTHENTICATION);
//        String userKey = JwtUtil.getUserKey(token);
//        String tokenKey = getTokenKey(userKey);
//        return redisTemplate.get(tokenKey);
//    }
//
//
//    private static String getTokenKey(String token) {
//        return "login_token:" + token;
//    }
//}
