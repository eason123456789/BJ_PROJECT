package bj.utils;

import cn.hutool.core.convert.Convert;
import com.auth0.jwt.JWT;
import com.auth0.jwt.JWTCreator;
import com.auth0.jwt.algorithms.Algorithm;
import com.auth0.jwt.exceptions.JWTVerificationException;
import com.auth0.jwt.interfaces.Claim;
import com.auth0.jwt.interfaces.DecodedJWT;
import org.springframework.stereotype.Component;

import java.util.Date;
import java.util.Map;

/**
 * @author eason
 */
@Component
public class JwtUtil {

    /**
     * 令牌自定义标识
     */
    public static final String AUTHENTICATION = "Authorization";

    /**
     * 令牌前缀
     */
    public static final String PREFIX = "Bearer ";

    /**
     * token过期时间,单位ms
     */
    public final static long EXPIRE_TIME=30*60*1000;

    /**
     * token签名
     */
    public final static String SIGN="msdsdasjdksladjasluieruh";

    /**
     * @Description:生成token
     * @Author: Eason
     * @Date: 2022/6/23 下午4:45
     * @param: map
     * @return: java.lang.String
     **/
    public static String CreateToken(Map<String, String> map){
        Date date=new Date(System.currentTimeMillis()+EXPIRE_TIME);
        JWTCreator.Builder builder = JWT.create();
        map.forEach((k,v)->{
            builder.withClaim(k,v);
        });
        String token = builder.withExpiresAt(date).sign(Algorithm.HMAC256(SIGN));
        return token;
    }

    /**
     * @Description:token的校验
     * @Author: Eason
     * @Date: 2022/6/23 下午5:07
     * @param: token
     * @return: boolean
     **/
    public  static boolean VerifyToken(String token){
        try {
            JWT.require(Algorithm.HMAC256(SIGN)).build()
                    .verify(token);
            return true;
        } catch (JWTVerificationException e) {
            e.printStackTrace();
            return false;
        } catch (IllegalArgumentException e) {
            e.printStackTrace();
            return false;
        }
    }

    /**
     * @Description: 获取用户信息
     * @Author: Eason
     * @Date: 2022/8/15 下午5:04
     * @param: Token
     * @return: void
     **/
    public static String getUserKey(String Token){
        return getTokenInFo(Token, SecurityConstants.USER_KEY);
    }
    public static String getUserId(String Token){
        return getTokenInFo(Token, SecurityConstants.DETAILS_USER_ID);
    }
    public static String getUserName(String Token){
        return getTokenInFo(Token, SecurityConstants.DETAILS_USERNAME);
    }


    /**
     * @Description: 获取token信息
     * @Author: Eason
     * @Date: 2022/8/12 下午4:22
     * @param: token
     * @return: com.auth0.jwt.interfaces.DecodedJWT
     **/
    public static   String getTokenInFo(String token,String key){
        DecodedJWT verify = JWT.require(Algorithm.HMAC256(SIGN)).build().verify(token);
        String asString = verify.getClaim(key).asString();
        return asString ;
    }

    public static String getTokenKey(String token) {
        return "login_token:" + token;
    }


//    public static void main(String[] args) {
//        Map<String, String> map=new HashMap<>();
//        map.put("username","eason");
//        map.put("password","1234567");
//        String s = CreateToken(map);
//        DecodedJWT tokenInFo = getTokenInFo(s);
//        System.out.println(tokenInFo.getClaim("username").asString());
//        System.out.println(s);
//        System.out.println(VerifyToken(s));
//    }

}
