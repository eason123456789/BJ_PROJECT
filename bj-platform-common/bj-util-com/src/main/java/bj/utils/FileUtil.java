package bj.utils;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

/**
 * @ClassName FileUtil
 * @Description 文件操作原生工具类
 * 不含apache-commons-io引用
 * @Author ctjsoft-pt
 * @Date 2022/11/12 12:37
 * @Versin 1.0
 **/
public class FileUtil {

    /**
     * 统计指定目录下文件中 所包含 指定字符串数量
     * @param filePath 文件路径
     * @param targetStr 目标字符串
     * @param split 字符分割符
     * @return
     * @throws IOException
     */
    public static Map<String, Integer> countFileSpecfic(String filePath, String targetStr, String split) throws IOException {
        String[] targetStrs = targetStr.split(split);
        LinkedHashMap<String, Integer> targetMap = IntStream.range(0, targetStrs.length)
                .boxed().collect(Collectors.toMap(k -> targetStrs[k], v -> 0, (k, v) -> v, LinkedHashMap<String, Integer>::new));
        return countFileLine(new File(filePath), targetMap);
    }


    /**
     * 统计源字符串下 指定目标字符串 出现数量
     *
     * @param sourceStr     源字符串
     * @param targetMap     目标字符串
     * @param caseSensitive 大小写敏感
     * @return
     */
    public static Map<String, Integer> countSpecific(String sourceStr, Map<String, Integer> targetMap, boolean caseSensitive) {
        Set<Map.Entry<String, Integer>> entries = targetMap.entrySet();
        for (Map.Entry<String, Integer> entry : entries) {
            String targetStr = entry.getKey();
            if (caseSensitive) {
                if (sourceStr.contains(targetStr)) {
                    entry.setValue(entry.getValue() + 1);
                }
            } else {
                String upperCase = targetStr.toUpperCase();
                String lowerCase = targetStr.toLowerCase();
                if (sourceStr.contains(lowerCase) || sourceStr.contains(upperCase)) {
                    entry.setValue(entry.getValue() + 1);
                }
            }

        }
        return targetMap;
    }

    /**
     * 统计文件路径下所有文件 指定目标字符串 出现数量
     *
     * @param file      文件路径
     * @param targetMap 指定字符串统计map
     * @return
     * @throws IOException
     */
    public static Map<String, Integer> countFileLine(File file, Map<String, Integer> targetMap) throws IOException {
        for (File containFile : file.listFiles()) {
            if (containFile.isDirectory()) {
                countFileLine(containFile, targetMap);
            } else {
                BufferedReader br = new BufferedReader(new FileReader(containFile));
                String sourceLine = null;
                while ((sourceLine = br.readLine()) != null) {
                    countSpecific(sourceLine, targetMap, false);
                }
                br.close();
            }
        }
        return targetMap;
    }


}
