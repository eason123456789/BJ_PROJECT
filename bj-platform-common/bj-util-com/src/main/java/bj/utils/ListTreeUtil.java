package bj.utils;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * @author eason
 */
public class ListTreeUtil {



   /**
    * @Description: 将集合组装成树
    * @Author: Eason
    * @Date: 2022/7/21 下午4:24
    * @param: mapList 数据集合
    * @param: map 子集数据
    * @param: id 主键ID
    * @param: top_id 主键ID名称
    * @param: parent_id 父级ID名称
    **/
    public static void TreeList(List<Map<String, Object>> mapList, Map<String, Object> map, String id, String top_id, String parent_id) {
        List<Map<String, Object>> childrenList = new ArrayList<>();
        for (Map<String, Object> org : mapList) {
            if (id.equals(String.valueOf(org.get(parent_id)))) {
                TreeList(mapList, org, String.valueOf(org.get(top_id)), top_id, parent_id);
                childrenList.add(org);
            }
        }
        if (childrenList != null && childrenList.size() > 0) {
            map.put("children", childrenList);
        }
    }

    public static void childrenTree(List<Map<String, Object>> mapList, Map<String, Object> map, String dept_id) {
        List<Map<String, Object>> childrenList = new ArrayList<>();
        for (Map<String, Object> org : mapList) {
            if (dept_id.equals(String.valueOf(org.get("parent_id")))) {
                childrenTree(mapList, org, String.valueOf(org.get("dept_id")));
                childrenList.add(org);
            }
        }
        if (childrenList != null && childrenList.size() > 0) {
            map.put("children", childrenList);
        }
    }

    public static void childrens(List<Map<String, Object>> mapList, Map<String, Object> mapdto, String id) {
        List<Map<String, Object>> mapList1 = new ArrayList<>();
        for (Map<String, Object> map : mapList) {
            if (map.get("parent_id").toString().equals(id)) {
                childrens(mapList, map, map.get("dept_id").toString());
                mapList1.add(map);
            }
        }
        mapdto.put("children", mapList1);
    }

}
