package bj.utils;

import org.springframework.util.StringUtils;

import java.util.List;
import java.util.Map;

/**
 * @author zhufei
 * @date 2024年02月19日 15:12
 */
public class SqlUtil {

    public static String sqlUtil(String tableName, List<String> fieldList, String alis, Map<String,Object> whereMap,
                                 String groupBy, String orderBy) {
        StringBuilder sql = new StringBuilder("SELECT ");
        if (!StringUtils.isEmpty(fieldList) && fieldList.size()>0) {
            for (String str : fieldList) {
                sql.append(str).append(",");
            }
            sql.replace(sql.length()-1,sql.length()," ");
        }else{
            sql.append(" * ");
        }
        sql.append(" FROM ").append(tableName).append(" ");
        if (!StringUtils.isEmpty(alis)) {
            sql.append(alis).append(" ");
        }
        sql.append(" WHERE 1 = 1 ");
        if (!whereMap.isEmpty()) {
            for (String key : whereMap.keySet()) {
                if ("1".equals(key)) {
                    sql.append(" AND ").append(whereMap.get(key));
                }else {
                    sql.append(" AND ").append(key.toUpperCase()).append(" = ").append(whereMap.get(key));
                }
            }
        }
        if (!StringUtils.isEmpty(groupBy)) {
            if (StringUtils.isEmpty(fieldList) && fieldList.size() <= 0 || fieldList.size() != groupBy.split(",").length ) {
                throw new RuntimeException("分组字段和查询字段不一致，检查语句是否正确！！");
            }
            sql.append(" ").append(groupBy).append(" ");
        }
        if (!StringUtils.isEmpty(orderBy)) {
            sql.append(" ").append(orderBy).append(" ");
        }
        return sql.toString();
    }
}
