package bj.observerPatternDemo;

/**
 * @ClassName ConcreteObserve
 * @Description 具体的观察者对象, 实现更新的方法, 使自身状态和目标状态一致
 * @Author ctjsoft-pt
 * @Date 2/6/2023 上午 10:45
 * @Versin 1.0
 **/
public class ConcreteObserve implements Observer {

    /**
     * 读者姓名
     */
    private String name;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public void update(Subject subject) {
        ConcreteSubject concreteSubject = (ConcreteSubject) subject;
        String content = concreteSubject.getContent();
        System.out.println(name + " 报纸内容收到了,内容是: " + content);
    }
}
