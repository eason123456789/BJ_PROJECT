package bj.observerPatternDemo;

import java.util.ArrayList;
import java.util.List;

/**
 * @ClassName Subject
 * @Description 目标对象, 他知道观察它的观察者, 并提供注册和删除观察者接口
 * @Author ctjsoft-pt
 * @Date 1/6/2023 上午 11:23
 * @Versin 1.0
 **/
public class Subject {

    /**
     * 用来保存注册的观察者对象 也就是报社的订阅者
     */
    private List<Observer> observerList = new ArrayList<>();

    /**
     * 注册 观察者对象  报社的读者需要向报社订阅
     *
     * @param observer 报纸的读者
     */
    public void attach(Observer observer) {
        observerList.add(observer);
    }

    /**
     * 删除观察者对象 报纸的读者可以取消订阅
     *
     * @param observer
     */
    public void detach(Observer observer) {
        observerList.remove(observer);
    }

    /**
     * 通知注册的所有观察者对象 每期报纸出版后,就要迅速送到读者手中
     */
    public void notifyObservers() {
        observerList.forEach(
                observer -> observer.update(this)
        );
    }


}
