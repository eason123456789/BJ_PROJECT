

-- ----------------------------
-- Table structure for bj_menu
-- ----------------------------
DROP TABLE IF EXISTS `bj_menu`;
CREATE TABLE `bj_menu` (
  `id` varchar(64) NOT NULL COMMENT '菜单ID',
  `name` varchar(255) NOT NULL COMMENT '菜单名称',
  `url` varchar(255) NOT NULL COMMENT '菜单url',
  `parent_id` varchar(64) NOT NULL COMMENT '父级菜单ID',
  `remark` varchar(255) DEFAULT NULL COMMENT '备注/说明',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for bj_role
-- ----------------------------
DROP TABLE IF EXISTS `bj_role`;
CREATE TABLE `bj_role` (
  `id` varchar(64) NOT NULL COMMENT '角色ID',
  `name` varchar(255) NOT NULL COMMENT '角色名称',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for bj_role_menu
-- ----------------------------
DROP TABLE IF EXISTS `bj_role_menu`;
CREATE TABLE `bj_role_menu` (
  `role_id` varchar(64) NOT NULL COMMENT '角色ID',
  `menu_id` varchar(64) NOT NULL COMMENT '菜单ID'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for bj_user
-- ----------------------------
DROP TABLE IF EXISTS `bj_user`;
CREATE TABLE `bj_user` (
  `id` varchar(64) NOT NULL COMMENT '用户ID',
  `name` varchar(255) NOT NULL COMMENT '用户名称',
  `code` varchar(255) NOT NULL COMMENT '用户编码',
  `password` varchar(255) NOT NULL COMMENT '用户密码',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for bj_user_role
-- ----------------------------
DROP TABLE IF EXISTS `bj_user_role`;
CREATE TABLE `bj_user_role` (
  `user_id` varchar(64) NOT NULL COMMENT '用户ID',
  `role_id` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '角色ID'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

SET FOREIGN_KEY_CHECKS = 1;

-- ----------------------------
-- Table structure for bj_dept
-- ----------------------------
DROP TABLE IF EXISTS `bj_dept`;
CREATE TABLE `bj_dept` (
  `dept_id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '部门id',
  `parent_id` bigint(20) DEFAULT '0' COMMENT '父部门id',
  `ancestors` varchar(50) DEFAULT '' COMMENT '祖级列表',
  `dept_name` varchar(30) DEFAULT '' COMMENT '部门名称',
  `order_num` int(11) DEFAULT '0' COMMENT '显示顺序',
  `leader` varchar(20) DEFAULT NULL COMMENT '负责人',
  `phone` varchar(11) DEFAULT NULL COMMENT '联系电话',
  `email` varchar(50) DEFAULT NULL COMMENT '邮箱',
  `status` char(1) DEFAULT '0' COMMENT '部门状态（0正常 1停用）',
  `del_flag` char(1) DEFAULT '0' COMMENT '删除标志（0代表存在 2代表删除）',
  `create_by` varchar(64) DEFAULT '' COMMENT '创建者',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(64) DEFAULT '' COMMENT '更新者',
  `update_time` datetime DEFAULT NULL COMMENT '更新时间',
  PRIMARY KEY (`dept_id`)
) ENGINE=InnoDB AUTO_INCREMENT=200 DEFAULT CHARSET=utf8 COMMENT='部门表';

SET FOREIGN_KEY_CHECKS = 1;

-- ----------------------------
-- Table structure for sys_logininfor
-- ----------------------------
DROP TABLE IF EXISTS `sys_logininfor`;
CREATE TABLE `sys_logininfor` (
  `info_id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '访问ID',
  `user_name` varchar(50) DEFAULT '' COMMENT '用户账号',
  `ipaddr` varchar(128) DEFAULT '' COMMENT '登录IP地址',
  `status` char(1) DEFAULT '0' COMMENT '登录状态（0成功 1失败）',
  `msg` varchar(255) DEFAULT '' COMMENT '提示信息',
  `access_time` datetime DEFAULT NULL COMMENT '访问时间',
  PRIMARY KEY (`info_id`)
) ENGINE=InnoDB AUTO_INCREMENT=152 DEFAULT CHARSET=utf8 COMMENT='系统访问记录';

SET FOREIGN_KEY_CHECKS = 1;


ALTER TABLE `bj_menu`
ADD COLUMN `perms` varchar(100) NULL COMMENT '权限标识' AFTER `remark`;
