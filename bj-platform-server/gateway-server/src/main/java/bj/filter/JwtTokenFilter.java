package bj.filter;

import bj.properties.IgnoreWhiteProperties;
import bj.utils.JwtUtil;
import bj.utils.RedisTemplateUtils;
import bj.utils.SecurityConstants;
import com.auth0.jwt.exceptions.TokenExpiredException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.gateway.filter.GatewayFilterChain ;
import org.springframework.cloud.gateway.filter.GlobalFilter;
import org.springframework.core.Ordered;
import org.springframework.http.server.reactive.ServerHttpRequest;
import org.springframework.stereotype.Component;
import org.springframework.util.AntPathMatcher;
import org.springframework.web.server.ServerWebExchange;
import reactor.core.publisher.Mono;

import java.util.Base64;
import java.util.List;
import java.util.Vector;

/**
 * @author eason
 */
@Component
public class JwtTokenFilter implements GlobalFilter, Ordered {

    Logger logger= LoggerFactory.getLogger(JwtTokenFilter.class);

    @Autowired
    private IgnoreWhiteProperties ignoreWhiteProperties;

    @Autowired
    private RedisTemplateUtils redisTemplate;

    private final AntPathMatcher pathMatcher=new AntPathMatcher();

    @Override
    public Mono<Void> filter(ServerWebExchange exchange, GatewayFilterChain chain) {
        ServerHttpRequest request = exchange.getRequest();
        ServerHttpRequest.Builder mutate = request.mutate();

        String path = request.getURI().getPath();
        List<String> whiteUrls = ignoreWhiteProperties.getWhiteUrls();
        //如果白名单里包含请求路径则放行
        for (String whiteUrl : whiteUrls) {
            if (pathMatcher.match(whiteUrl,path)){
                return chain.filter(exchange);
            }
        }
        String token = request.getHeaders().getFirst(JwtUtil.AUTHENTICATION);
        if (token==null || token.equals("")){
            logger.info("令牌不能为空");
            throw new TokenExpiredException("令牌不能为空");
        }
        if (!JwtUtil.VerifyToken(token)){
            logger.info("令牌过期");
            throw new TokenExpiredException("令牌过期");
        }
        String userKey = JwtUtil.getUserKey(token);
        String userId = JwtUtil.getUserId(token);
        String userName = JwtUtil.getUserName(token);
        String tokenKey = getTokenKey(userKey);
        boolean b = redisTemplate.hasKey(tokenKey);
        if (!b){
            logger.info("登录状态已过期");
            throw new TokenExpiredException("登录状态已过期");
        }
        //对放进请求头的信息进行加密处理
        String tokenKeys = Base64.getEncoder().encodeToString(tokenKey.getBytes());
        String userIds = Base64.getEncoder().encodeToString(userId.getBytes());
        String userNames = Base64.getEncoder().encodeToString(userName.getBytes());
        addHeader(mutate,SecurityConstants.USER_KEY,tokenKeys);
        addHeader(mutate, SecurityConstants.DETAILS_USER_ID,userIds);
        addHeader(mutate,SecurityConstants.DETAILS_USERNAME,userNames);
        
        return chain.filter(exchange.mutate().request(mutate.build()).build());
    }

    private void addHeader(ServerHttpRequest.Builder mutate,String name,Object value){
        if (value==null){
            return;
        }
        mutate.header(name,value.toString());
    }

    private String getTokenKey(String token){
        return "login_token:" + token;
    }

    @Override
    public int getOrder() {
        return -200;
    }
}
