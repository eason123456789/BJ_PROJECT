package bj.properties;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

import java.util.ArrayList;
import java.util.List;

/**
 * @author eason
 */
@Configuration
@ConfigurationProperties(prefix = "sso.ignore")
public class IgnoreWhiteProperties {

    private List<String> WhiteUrls = new ArrayList<>();


    public List<String> getWhiteUrls() {
        return WhiteUrls;
    }

    public void setWhiteUrls(List<String> whiteUrls) {
        WhiteUrls = whiteUrls;
    }
}
