//导入vue
import Vue from 'vue';
import VueRouter from 'vue-router';
//导入组件
import Main from "../views/Main";
import Login from "../views/Login";
import NotFound from "../views/NotFound";
import List from "../views/user/List";
import Profile from "../views/user/Profile";
import Eason from "../components/Eason";
//使用
Vue.use(VueRouter);
//导出
export default new VueRouter({
  routes: [
    {
      //登录页
      path: '/main',
      component: Main
    },
    //首页
    {
      path: '/',
      component: Login
    },
    {
      path: '/eason',
      component: Eason
    },
    {
      path: '/user/list',
      component: List
    },
    {
      path: '/user/Profile/:id',
      name:'Profile',
      component: Profile,
      props: true
    },
    {
      path: '*',
      component: NotFound
    }
  ]

})

