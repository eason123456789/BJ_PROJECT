//package bj.service;
//
//import bj.mapper.UserInfoMapper;
//import bj.mapper.UserMapper;
//import bj.po.Role;
//import bj.po.User;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.security.core.GrantedAuthority;
//import org.springframework.security.core.authority.AuthorityUtils;
//import org.springframework.security.core.authority.SimpleGrantedAuthority;
//import org.springframework.security.core.userdetails.UserDetails;
//import org.springframework.security.core.userdetails.UserDetailsService;
//import org.springframework.security.core.userdetails.UsernameNotFoundException;
//import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
//import org.springframework.stereotype.Service;
//
//import java.util.ArrayList;
//import java.util.List;
//
//@Service
//public class LoginService implements UserDetailsService {
//
//    @Autowired
//    private UserMapper userMapper;
//
//    @Autowired
//    private UserInfoMapper userInfoMapper;
//
//    @Override
//    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
//        BCryptPasswordEncoder bCryptPasswordEncoder = new BCryptPasswordEncoder();
//        User user = userMapper.getUserpassword(username);
//        if (user==null){
//            throw new UsernameNotFoundException("用户名不存在！！");
//        }
//        List<Role> roleList = userInfoMapper.selectRoleByUserId(user.getId());
//
//        List<GrantedAuthority> grantedAuthorityList=new ArrayList<>();
//
//        for (Role role : roleList) {
//            SimpleGrantedAuthority simpleGrantedAuthority=new SimpleGrantedAuthority("ROLE_"+role.getName());
//            grantedAuthorityList.add(simpleGrantedAuthority);
//        }
//        String password = bCryptPasswordEncoder.encode(user.getPassword());
//        return new org.springframework.security.core.userdetails.User
//                (user.getName(),password, grantedAuthorityList);
//    }
//
////    @Override
////    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
////        BCryptPasswordEncoder bCryptPasswordEncoder = new BCryptPasswordEncoder();
////        if (!"eason".equals(username)) {
////            throw new UsernameNotFoundException("用户名不存在！！");
////        }
////        String password = bCryptPasswordEncoder.encode("123456");
////
////        return new User(username,password, AuthorityUtils.commaSeparatedStringToAuthorityList("admin,"));
////    }
//
//
//
//}
