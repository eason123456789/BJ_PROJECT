package bj.service;

import bj.api.ILogInfoController;
import bj.api.IUserController;
import bj.feign.LoginInfoFeign;
import bj.po.LoginInfoPo;
import bj.po.User;
import cn.hutool.extra.servlet.ServletUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import javax.servlet.http.HttpServletRequest;
import java.util.Objects;

/**
 * @author eason
 */
@Component
public class LoginLogService {

    @Autowired
    private ILogInfoController loginInfoFeign;

    /**
     * @Description: 记录登录日志
     * @Author: Eason
     * @Date: 2022/8/23 下午5:18
     * @param: user
     * @return: void
     **/
    public void recordLogininfor(String username, String status, String message){
        HttpServletRequest request = ((ServletRequestAttributes) Objects
                .requireNonNull(RequestContextHolder.getRequestAttributes()))
                .getRequest();

        LoginInfoPo loginInfoPo=new LoginInfoPo();

        loginInfoPo.setUserName(username);
        loginInfoPo.setStatus(status);
        loginInfoPo.setMsg(message);
        loginInfoPo.setIpaddr(ServletUtil.getClientIP(request).equals("0:0:0:0:0:0:0:1")?"127.0.0.1":ServletUtil.getClientIP(request));

        loginInfoFeign.addLoginInfo(loginInfoPo);
    }
}
