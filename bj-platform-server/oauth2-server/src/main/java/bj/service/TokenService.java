package bj.service;

import bj.api.IUserController;
import bj.po.LoginUser;
import bj.po.User;
import bj.utils.JwtUtil;
import bj.utils.RedisTemplateUtils;
import bj.utils.SecurityConstants;
import cn.hutool.core.lang.UUID;
import cn.hutool.extra.servlet.ServletUtil;
import cn.hutool.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;
import java.util.concurrent.TimeUnit;

/**
 * @author eason
 */
@Service
public class TokenService {

    private static final Logger logger= LoggerFactory.getLogger(TokenService.class);

    @Autowired
    private IUserController userFeign;

    @Autowired
    private RedisTemplateUtils redisTemplate;

    @Autowired
    private LoginLogService loginLogService;

    @Autowired
    private checkNameAndPass checkNameAndPass;


    /**
     * @Description: 用户的登录功能
     * @Author: Eason
     * @Date: 2022/8/16 上午9:25
     * @param: username
     * @param: password
     * @return: bj.po.User
     **/
    public LoginUser login(String username,String password) throws Exception {
        LoginUser loginUser = userFeign.selectUserByUserName(username).getData();
        User user = loginUser.getUser();
        BCryptPasswordEncoder passwordEncoder = new BCryptPasswordEncoder();
        boolean matches = passwordEncoder.matches(password, user.getPassword());
        if (matches){
            logger.info("登录成功！！！");
            loginLogService.recordLogininfor(username,"0","登录成功");
            return loginUser;
        }else {
            checkNameAndPass.validate(username,password);
            logger.info("登录失败！！！");
            loginLogService.recordLogininfor(username,"1","登录失败");
            return null;
        }

    }

    /**
     * @Description: 用户的退出功能
     * @Author: Eason
     * @Date: 2022/8/16 上午9:24
     * @param: token
     * @return: void
     **/
    public void logout(String token){
        boolean b = JwtUtil.VerifyToken(token);
        if (b){
            String userKey = JwtUtil.getUserKey(token);
            if (userKey!=null && !userKey.isEmpty()) {
                String tokenKey = getTokenKey(userKey);
                redisTemplate.delete(tokenKey);
            }
        }else {
            logger.info("请先登录！！！");
        }
    }

    /**
     * @Description:创建Token
     * @Author: Eason
     * @Date: 2022/8/12 下午3:15
     * @param: loginUser
     * @return: java.util.Map<java.lang.String,java.lang.Object>
     **/
    public String createToken(LoginUser loginUser){
        String tokenId = UUID.randomUUID().toString();
        refreshToken(loginUser,tokenId);
        Map<String, String> map=new HashMap<>();
        map.put(SecurityConstants.USER_KEY,tokenId);
        map.put(SecurityConstants.DETAILS_USER_ID,loginUser.getUser().getId());
        map.put(SecurityConstants.DETAILS_USERNAME,loginUser.getUser().getName());
        String token = JwtUtil.CreateToken(map);
        return token;
    }



    public void refreshToken(LoginUser loginUser,String tokenId){
        HttpServletRequest request = ((ServletRequestAttributes) Objects
                .requireNonNull(RequestContextHolder.getRequestAttributes()))
                .getRequest();
        loginUser.setUserid(loginUser.getUser().getId());
        loginUser.setUsername(loginUser.getUser().getName());
        loginUser.setToken(tokenId);
        loginUser.setIpaddr(ServletUtil.getClientIP(request).equals("0:0:0:0:0:0:0:1")?"127.0.0.1":ServletUtil.getClientIP(request));
        loginUser.setLoginTime(System.currentTimeMillis());
        loginUser.setExpireTime(loginUser.getLoginTime()+30*60*1000);
        String tokenKey = getTokenKey(tokenId);
        redisTemplate.set(tokenKey,loginUser,30,TimeUnit.MINUTES);
    }

    private String getTokenKey(String token) {
        return "login_token:" + token;
    }
}
