package bj.service;

import bj.utils.CacheConstants;
import bj.utils.RedisTemplateUtils;
import bj.utils.SecurityConstants;
import com.google.protobuf.ServiceException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.concurrent.TimeUnit;

/**
 * @author eason
 */
@Service
public class checkNameAndPass {
    @Autowired
    private RedisTemplateUtils redisTemplateUtils;

    private final int maxCount= CacheConstants.PASSWORD_MAX_RETRY_COUNT;

    private final Long lockTime = CacheConstants.PASSWORD_LOCK_TIME;


    /**
     * @Description:获取缓存Key
     * @Author: Eason
     * @Date: 2023/3/6 下午3:35
     * @param: username
     * @return: java.lang.String
     **/
    private String getCacheKey(String username) {
        return CacheConstants.PWD_ERR_CNT_KEY + username;
    }

    /**
     * @Description:校验用户名密码错误次数
     * @Author: Eason
     * @Date: 2023/3/6 下午3:51
     * @param: userName
     * @param: password
     * @return: void
     **/
    public void validate(String userName,String password) throws Exception {
        Integer count=redisTemplateUtils.getCacheObject(getCacheKey(userName));
        if (count==null){
            count=0;
        }
        if (count >= maxCount) {
            String errMsg = String.format("密码输入错误%s次，帐户锁定%s分钟", maxCount, lockTime);
            throw new Exception(errMsg);
        }
        if (count < maxCount) {
            count += 1;
            redisTemplateUtils.set(getCacheKey(userName), count, lockTime, TimeUnit.MINUTES);
            throw new Exception("用户名/密码错误");
        } else {
            if (redisTemplateUtils.hasKey(getCacheKey(userName))) {
                redisTemplateUtils.delete(getCacheKey(userName));
            }
        }
    }
}
