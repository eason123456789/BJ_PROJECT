package bj.mapper;


import bj.po.User;
import org.springframework.stereotype.Repository;

/**
 * @author eason
 */
@Repository
public interface UserMapper {

    public User getUserpassword(String username);
}
