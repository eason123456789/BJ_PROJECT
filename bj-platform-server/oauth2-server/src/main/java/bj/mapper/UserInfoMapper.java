package bj.mapper;

import bj.po.Role;
import org.springframework.stereotype.Repository;

import java.util.List;


@Repository
public interface UserInfoMapper {

   List<Role> selectRoleByUserId(String user_id);
}
