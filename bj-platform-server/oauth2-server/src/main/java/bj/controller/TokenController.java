package bj.controller;

import bj.core.ResultData;
import bj.po.LoginUser;
import bj.po.TokenUser;
import bj.service.TokenService;
import bj.utils.JwtUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;

/**
 * @author eason
 */
@RestController
@RequestMapping(value = "/login")
public class TokenController {
    private static final Logger logger = LoggerFactory.getLogger(TokenController.class);

    @Autowired
    private TokenService tokenService;



    /**
     * @Description: 用户的登录功能
     * @Author: Eason
     * @Date: 2022/8/16 上午9:26
     * @param: user
     * @return: bj.core.ResultData<java.lang.String>
     **/
    @PostMapping(value = "/tologin")
    public ResultData<String> login(@RequestBody TokenUser user){
        try {
            String name = user.getUsername();
            String password = user.getPassword();
            LoginUser loginUser = tokenService.login(name, password);
            String token = tokenService.createToken(loginUser);
            return new ResultData("0000","登录成功",token);
        } catch (Exception e) {
            e.printStackTrace();
            return new ResultData("9999",e.getMessage());
        }

    }

    /**
     * @Description: 用户的退出功能
     * @Author: Eason
     * @Date: 2022/8/16 上午9:26
     * @param: request
     * @return: bj.core.ResultData
     **/
    @DeleteMapping(value = "/logout")
    public ResultData logout(HttpServletRequest request){
        String token = request.getHeader(JwtUtil.AUTHENTICATION);
        tokenService.logout(token);
        return new ResultData("0000","退出成功",token);
    }
}
