package bj.po;

import lombok.Data;

/**
 * @author eason
 */
@Data
public class TokenUser {

    private String username;

    private String password;
}
