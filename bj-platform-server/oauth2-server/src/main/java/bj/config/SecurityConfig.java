//package bj.config;
//
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.context.annotation.Bean;
//import org.springframework.context.annotation.Configuration;
//import org.springframework.security.config.annotation.web.builders.HttpSecurity;
//import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
//import org.springframework.security.core.userdetails.UserDetailsService;
//import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
//import org.springframework.security.crypto.password.PasswordEncoder;
//import org.springframework.security.web.authentication.rememberme.JdbcTokenRepositoryImpl;
//import org.springframework.security.web.authentication.rememberme.PersistentTokenRepository;
//
//import javax.sql.DataSource;
//
//@Configuration
//public class SecurityConfig extends WebSecurityConfigurerAdapter {
//
//    @Autowired
//    private DataSource dataSource;
//    @Autowired
//    private UserDetailsService userDetailsService;
//
//    @Override
//    protected void configure(HttpSecurity http) throws Exception {
//        http.formLogin()
//                .loginPage("/login.html")
//                .loginProcessingUrl("/user/login")
//                .defaultSuccessUrl("/success.html").permitAll()
//                //需要用户带有管理员权限
//                .and().authorizeRequests().antMatchers("/index").hasRole("管理员")
//                //需要主体带有role权限
//                .antMatchers("/findAll").hasAnyAuthority("role")
//                .anyRequest().authenticated()
//                .and().csrf().disable();
//
//        http.rememberMe()
//                .tokenRepository(tokenRepository())
//                .userDetailsService(userDetailsService);
//
//        http.logout().logoutUrl("/logout")
//                .logoutSuccessUrl("/success").permitAll();
//    }
//
//    /**
//     * @Description:将PasswordEncoder注入到spring容器
//     * @Author: Eason
//     * @Date: 2022/7/11 下午3:11
//     * @return: org.springframework.security.crypto.password.PasswordEncoder
//     **/
//    @Bean
//    public PasswordEncoder passwordEncoder(){
//        return new BCryptPasswordEncoder();
//    }
//
//    /**
//     * @Description:记住我功能
//     * @Author: Eason
//     * @Date: 2022/7/18 上午11:16
//     * @return: org.springframework.security.web.authentication.rememberme.PersistentTokenRepository
//     **/
//    @Bean
//    public PersistentTokenRepository tokenRepository(){
//
//        JdbcTokenRepositoryImpl jdbcTokenRepository=new JdbcTokenRepositoryImpl();
//
//        jdbcTokenRepository.setDataSource(dataSource);
//        //自动创建token表
//        //jdbcTokenRepository.setCreateTableOnStartup(true);
//        return jdbcTokenRepository;
//    }
//
//
//}
