package bj.config;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;

@Configuration
public class SwaggerConfig {
    @Value("${spring.application.name}")
    private String name;


    @Value("${swagger.scanPackage}")
    private String swaggerScanPackage;

    @Value("${swagger.apiVersion}")
    private String version;
    @Bean
    public Docket createRestApi() {
        return new Docket(DocumentationType.SWAGGER_2)
                .groupName(name)
                .pathMapping("/")
                .select()
                .apis(RequestHandlerSelectors.basePackage(swaggerScanPackage))
                .paths(PathSelectors.any())
                .build()
                .apiInfo(getApiInfoBuilder());
    }

    public ApiInfo getApiInfoBuilder(){
       return new ApiInfoBuilder()
                .title(name+"服务提供的RESTful APIs")
                .description("BJ服务API")
                .version(version)
                .build();
    }
}
