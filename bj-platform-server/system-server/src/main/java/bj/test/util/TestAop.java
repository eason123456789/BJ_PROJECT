package bj.test.util;

import java.lang.annotation.*;

/**
 * @author eason
 */
@Target(ElementType.METHOD)
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface TestAop {
}
