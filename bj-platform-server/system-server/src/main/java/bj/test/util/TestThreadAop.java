package bj.test.util;

import lombok.SneakyThrows;
import lombok.Synchronized;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.springframework.stereotype.Component;

/**
 * @author eason
 */
@Aspect
@Component
public class TestThreadAop {

    @SneakyThrows
    @Around(value = "@annotation(TestAop)")
    public void around(ProceedingJoinPoint point) {
        new Thread (()->  {
            try {
                point.proceed();
            } catch (Throwable throwable) {
                throwable.printStackTrace();
            }
        }).start();
    }
}
