package bj.test;

import bj.test.util.TestAop;
import lombok.extern.slf4j.Slf4j;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;

/**
 * @author eason
 */
@Service
@Slf4j
public class TestService {

    @TestAop
    public void test01(){
        log.info("2");
    }

    @Async
    public void add(){
        try {
            Thread.sleep(3000);
            log.info("2");
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}
