package bj.test;

import bj.test.util.TestAop;
import cn.hutool.log.Log;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Async;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author eason
 */
@Slf4j
@RestController
@RequestMapping("test")
public class TestController {
    @Autowired
    private TestService testService;


    @GetMapping("/test01")
    public String getTest(){
        log.info("1");
        testService.test01();
        log.info("3");
        return "Success";
    }

    @GetMapping("/test02")
    public String getTest2(){
        log.info("1");
        testService.add();
        log.info("3");
        return "Success";
    }


}
