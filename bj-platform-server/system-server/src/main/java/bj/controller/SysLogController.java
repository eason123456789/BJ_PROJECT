package bj.controller;

import bj.api.ILogController;
import bj.core.ResultData;
import bj.po.SysLogPo;
import bj.service.impl.SysLogServiceImpl;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author eason
 */
@RestController
@RequestMapping("/restapi/log")
public class SysLogController implements ILogController{

    private static final Logger logger= LoggerFactory.getLogger(SysLogController.class);

    @Autowired
    private SysLogServiceImpl sysLogService;

    @PostMapping("/add")
    @Override
    public ResultData<Integer> saveLog(@RequestBody SysLogPo sysLogPo) {
        try {
            return new ResultData("0000","操作成功",sysLogService.saveLog(sysLogPo));
        } catch (RuntimeException e){
            logger.error(e.getMessage(),e);
            return new ResultData("9999","操作失败",null);
        }
    }
}
