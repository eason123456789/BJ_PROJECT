package bj.controller;

import bj.core.ResultData;
import bj.po.LoginUser;
 import bj.utils.RedisTemplateUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.*;

/**
 * @author eason
 */
@RestController
@RequestMapping(value = "/cache")
public class CacheController {

    private final Logger logger= LoggerFactory.getLogger(CacheController.class);

    @Autowired
    private RedisTemplateUtils redisTemplate;



    /**
     * @Description: 获取redis缓存中的用户
     * @Author: Eason
     * @Date: 2022/10/8 下午4:31
     * @param: key
     * @return: bj.core.ResultData<java.util.Map<java.lang.String,java.lang.Object>>
     **/
    @GetMapping(value = "/key/value")
    public ResultData<Map<String, Object>> getRedisKeyValue(@RequestParam("key") String key){
        Map<String, Object> map= null;
        try {
            map = new HashMap<>();
            Set<String> scan = redisTemplate.scan(key);
            for (String s : scan) {
                LoginUser loginUser = redisTemplate.getCacheObject(s);
                map.put(loginUser.getUserid(),loginUser.getUser());
            }
            return new ResultData<>("0000","查询成功",map);
        } catch (Exception e) {
            logger.info( e.getMessage(),e);
            return new ResultData("9999","查询失败");
        }
    }


}
