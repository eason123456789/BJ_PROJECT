package bj.controller;

import bj.api.ILogController;
import bj.core.ResultData;
import bj.po.SysLogPo;
import bj.util.RocketmqStreamUtil;
import com.alibaba.fastjson2.JSON;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.stereotype.Component;

    @Component
    @ConditionalOnProperty(prefix="ctj.operation.log",name="strategy",havingValue="rocketmq")
    public class SysLogMqController implements ILogController {
        private static final Logger log = LoggerFactory.getLogger(SysLogMqController.class);
        @Autowired
        RocketmqStreamUtil rocketmqStreamUtil;

        @Override
        public ResultData<Integer> saveLog(SysLogPo sysLogPo) {
            try{
                String log = JSON.toJSONString(sysLogPo);
                rocketmqStreamUtil.sendMessage(log);
            }catch (Exception e){
                log.error("使用rocketmq发送操作日志消息异常",e);
            }
            return null;
        }
}
