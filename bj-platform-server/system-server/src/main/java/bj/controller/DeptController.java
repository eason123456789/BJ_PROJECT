package bj.controller;

import bj.annoation.SysLog;
import bj.core.ResultData;
import bj.service.DeptService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.Map;

/**
 * @author eason
 */
@RestController
@Api(tags = "部门管理相关接口")
@RequestMapping(value = "/restapi/dept")
public class DeptController {

    private static final Logger logger = LoggerFactory.getLogger(DeptController.class);

    @Autowired
    private DeptService deptService;


    @SysLog("查询部门树")
    @GetMapping(value = "/tree")
    @ApiOperation("查询部门树")
    public ResultData<List<Map<String, Object>>> getDeptTree(){
        try {
            List<Map<String, Object>> deptTree = deptService.getDeptTree();
            return new ResultData("0000","查询成功",deptTree,deptTree.size());
        } catch (RuntimeException e) {
            logger.error(e.getMessage(),e);
            return new ResultData("9999","查询失败",null);
        }

    }

}
