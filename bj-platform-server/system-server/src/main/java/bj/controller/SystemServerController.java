package bj.controller;


import bj.core.ResultData;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.client.ServiceInstance;
import org.springframework.cloud.client.discovery.DiscoveryClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author eason
 */
@RequestMapping(value = "/systemServer")
@Api(tags = "微服务管理相关接口")
@RestController
public class SystemServerController {
    private static final Logger logger = LoggerFactory.getLogger(SystemServerController.class);
    @Autowired
    DiscoveryClient discoveryClient;

    @ApiOperation("查询服务列表")
    @GetMapping(value ="/list")
    public ResultData<Map<String,List<ServiceInstance>>> getUser(){
        try {
            List<String> services = discoveryClient.getServices();
            Map<String, List<ServiceInstance>> msl = new HashMap<>();
            for (String service : services) {
                List<ServiceInstance> instances = discoveryClient.getInstances(service);
                msl.put(service,instances);
            }
            return new ResultData("0000","查询成功",msl,msl.size());
        }catch (RuntimeException e){
            logger.error(e.getMessage(),e);
            return new ResultData("9999","查询失败",null);
        }
    }
}
