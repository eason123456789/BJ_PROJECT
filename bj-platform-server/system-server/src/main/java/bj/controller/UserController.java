package bj.controller;

import bj.annoation.SysLog;
import bj.api.IUserController;
import bj.common.security.annotation.RequiresPermissions;
import bj.common.security.utils.SecurityUtil;
import bj.core.ResultData;
import bj.po.LoginUser;
import bj.po.User;
import bj.service.RoleMenuService;
import bj.service.UserRoleService;
import bj.service.UserService;
import bj.utils.JwtUtil;
import bj.utils.RedisTemplateUtils;
import bj.utils.SecurityConstants;
import io.netty.util.internal.InternalThreadLocalMap;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.apache.commons.codec.binary.Base64;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.nio.charset.Charset;
import java.util.*;

/**
 * @author eason
 */
@RestController
@Api(tags = "用户管理相关接口")
public class UserController implements IUserController {

    private static final Logger logger = LoggerFactory.getLogger(UserController.class);

    @Autowired
    private UserService userService;

    @Autowired
    private UserRoleService userRoleService;

    @Autowired
    private RoleMenuService roleMenuService;

    @Autowired
    private RedisTemplateUtils redisTemplate;

    /**
     * @Description:查询用户列表
     * @Author: Eason
     * @Date: 2022/7/11 上午10:41
     * @return: bj.core.ResultData<java.util.List<java.util.Map<java.lang.String,java.lang.Object>>>
     **/
    @ApiOperation("查询用户列表")
    @GetMapping(value ="/list")
    @SysLog("查询用户列表")
    @RequiresPermissions("system:user:list")
    public ResultData<List<User>> getUser(){
        try {
            List<User> user = userService.getUser();
            return new ResultData("0000","查询成功",user,user.size());
        }catch (RuntimeException e){
            logger.error(e.getMessage(),e);
            return new ResultData("9999","查询失败",null);
        }
    }
    /**
     * @Description:查询用户列表(分页)
     * @Author: Eason
     * @Date: 2022/7/11 上午10:41
     * @return: bj.core.ResultData<java.util.List<java.util.Map<java.lang.String,java.lang.Object>>>
     **/
    @ApiOperation("查询用户列表(分页)")
    @GetMapping(value ="/conditions")
    public ResultData<List<User>> getUserByPageInfo(@RequestParam(value = "page", required = false, defaultValue = "1") int page,
                                                    @RequestParam(value = "pageSize", required = false, defaultValue = "10") int pageSize,
                                                    @RequestBody(required = false) Map<String, Object> params){
        try {
            List<User> userByPageInfo = userService.getUserByPageInfo(page, pageSize, params);
            return new ResultData<>("0000","查询成功",userByPageInfo,userByPageInfo.size(),page,pageSize);
        }catch (RuntimeException e){
            logger.error(e.getMessage(),e);
            return new ResultData("9999","查询失败",null);
        }
    }

    /**
     * @Description:添加用户
     * @Author: Eason
     * @Date: 2022/7/14 下午2:40
     * @param: user
     * @return: bj.core.ResultData<java.lang.Void>
     **/
    @ApiOperation("添加用户")
    @PostMapping(value = "/add")
    public ResultData<Void> insertUser(@RequestBody User user){
        try {
            userService.insertUser(user);
            return new ResultData("0000","添加成功",user);
        }catch (RuntimeException e){
            logger.error(e.getMessage(),e);
            return new ResultData("9999","添加失败",null);
        }
    }

   /**
    * @Description:查询用户(根据用户ID)
    * @Author: Eason
    * @Date: 2022/7/19 下午5:05
    * @param: id
    * @return: bj.core.ResultData<bj.po.User>
    **/
    @ApiOperation("查询用户(根据用户ID)")
    @GetMapping(value = "/getUserById")
    public ResultData<User> getUserById(@RequestParam String id){
        try {
            User user = userService.getUserById(id);
            return new ResultData("0000","查询成功",user);
        }catch (RuntimeException e){
            logger.error(e.getMessage(),e);
            return new ResultData("9999","查询失败",null);
        }
    }

    /**
     * @Description: 根据用户名密码查询用户
     * @Author: Eason
     * @Date: 2022/8/17 下午4:37
     * @param: userName
     * @return: bj.core.ResultData<bj.po.User>
     **/
    @Override
    @ApiOperation("查询用户(根据用户名)")
    @GetMapping(value = "/getUserByName")
    public ResultData<LoginUser> selectUserByUserName(@RequestParam(value = "username") String userName){
        try {
            User user = userService.selectUserByUserName(userName);
            if (user==null){
                return new ResultData("9999","用户名密码错误",null);
            }
            Set<Integer> roleByUserId = userRoleService.getRoleByUserId(user.getId());
            Set<String> menuByUserId = roleMenuService.getMenuByUserId(user.getId());
            LoginUser loginUser=new LoginUser();
            loginUser.setRoles(roleByUserId);
            loginUser.setUser(user);
            loginUser.setPermissions(menuByUserId);
            return new ResultData("0000","查询成功",loginUser);
        }catch (RuntimeException e){
            logger.error(e.getMessage(),e);
            return new ResultData("9999","查询失败",null);
        }
    }


    /**
     * @Description: 查询当前登录用户信息
     * @Author: Eason
     * @Date: 2022/9/14 上午9:41
     * @param: request
     * @return: bj.core.ResultData<bj.po.LoginUser>
     **/
    @ApiOperation("查询当前登录用户信息")
    @GetMapping("/user")
    public ResultData<LoginUser> getUserInfo(HttpServletRequest request){
        try {
            LoginUser loginUser=new LoginUser();
            String userId = SecurityUtil.getUserId();
            loginUser.setUser(userService.getUserById(userId));
            loginUser.setRoles(userRoleService.getRoleByUserId(userId));
            loginUser.setPermissions(roleMenuService.getMenuByUserId(userId));
            return new ResultData("0000", "查询成功", loginUser);
        }catch (RuntimeException e){
            logger.error(e.getMessage(),e);
            return new ResultData("9999","查询失败",null);
        }
    }


    @PostMapping(value = "/test")
    public ResultData<Void> Test(@RequestBody List<Object> list){
        try {
            List<Object> test = userService.test(list);
            return new ResultData("0000","添加成功",test);
        }catch (RuntimeException e){
            logger.error(e.getMessage(),e);
            return new ResultData("9999","添加失败",null);
        }
    }



}
