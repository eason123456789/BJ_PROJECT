package bj.controller;

import bj.core.ResultData;
import bj.po.Role;
import bj.service.RoleService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@Api(tags = "角色管理相关接口")
@RestController
@RequestMapping(value = "restapi/role")
public class RoleController {

    private static final Logger logger = LoggerFactory.getLogger(RoleController.class);

    @Autowired
    private RoleService roleService;


    @ApiOperation("角色列表查询")
    @GetMapping("/list")
    public ResultData<List<Role>> getRole(){
        try {
            List<Role> roleList = roleService.getRole();
            return new ResultData("0000","查询成功",roleList,roleList.size());
        } catch (RuntimeException e){
            logger.error(e.getMessage(),e);
            return new ResultData("9999","查询失败",null);
        }

    }

}
