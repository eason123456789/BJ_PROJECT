package bj.controller;


import org.springframework.web.bind.annotation.*;

import java.util.Arrays;
import java.util.List;

/**
 * @ClassName SecurityInterfaceController
 * @Description 接口加密测试类
 * @Author ctjsoft-pt
 * @Date 2022/10/17 9:10
 * @Versin 1.0
 **/
@RestController
@RequestMapping("/security")
public class SecurityInterfaceController {


    @GetMapping("/security")
    public String secTest(@RequestParam String str) {
//        List<String> objects = Arrays.asList(new String[]{"1", "2", "5", "7", "9", "10"});
        String objects = str;
        System.out.println("==========================="+objects+"===========================");
        return objects;
    }


}
