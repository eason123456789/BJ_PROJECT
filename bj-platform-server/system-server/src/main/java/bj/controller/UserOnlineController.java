package bj.controller;


import bj.core.ResultData;
import bj.po.LoginUser;
import bj.po.UserOnlinePo;
import bj.service.UserOnlineService;
import bj.utils.RedisTemplateUtils;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

/**
 * @author eason
 */
@Api(tags = "在线用户")
@RequestMapping(value = "/online")
@RestController
public class UserOnlineController {

    private final Logger logger=LoggerFactory.getLogger(UserOnlineController.class);

    @Autowired
    private RedisTemplateUtils redisTemplate;

    @Autowired
    private UserOnlineService userOnlineService;

    /**
     * @Description: 在线用户列表查询
     * @Author: Eason
     * @Date: 2022/8/24 下午3:13
     * @return: bj.core.ResultData<java.util.List<bj.po.UserOnlinePo>>
     **/
    @ApiOperation("在线用户列表查询")
    @GetMapping("/list")
    public ResultData<List<UserOnlinePo>> getOnlineUser(){

        try {
            List<UserOnlinePo> userOnlineList = new ArrayList<UserOnlinePo>();
            //Collection<String> keys = redisTemplate.keys("login_token:"+"*");
            Set<String> scan = redisTemplate.scan("login_token:" + "*");
            for (String key : scan) {
                LoginUser user = redisTemplate.getCacheObject(key);
                UserOnlinePo onlineUser = userOnlineService.getOnlineUser(user);
                userOnlineList.add(onlineUser);
            }
            return new ResultData<>("0000","查询成功",userOnlineList,userOnlineList.size());
        } catch (RuntimeException e) {
            logger.info(e.getMessage(),e);
            return new ResultData<>("9999","查询失败",null);
        }
    }

    @ApiOperation("在线用户列表删除")
    @DeleteMapping(value = "/{tokenId}")
    public ResultData delOnlineUser(@PathVariable("tokenId") String tokenId){
        try {
            String tokenKey = getTokenKey(tokenId);
            redisTemplate.delete(tokenKey);
            return new ResultData("0000","操作成功");
        } catch (RuntimeException e) {
            logger.info( e.getMessage(),e);
            return new ResultData("9999","操作失败");
        }

    }

    private String getTokenKey(String token)
    {
        return "login_token:" + token;
    }

}
