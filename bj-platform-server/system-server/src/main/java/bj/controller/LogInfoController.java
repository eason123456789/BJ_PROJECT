package bj.controller;

import bj.api.ILogInfoController;
import bj.core.ResultData;
import bj.po.LoginInfoPo;
import bj.service.LoginInfoService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

/**
 * @author eason
 * 登录日志记录
 */
@RestController
@Api(tags = "登录日志")
@RequestMapping(value = "/restapi/logininfo")
public class LogInfoController implements ILogInfoController {

    private Logger logger= LoggerFactory.getLogger(LogInfoController.class);

    @Autowired
    private LoginInfoService loginInfoService;

    @Override
    @ApiOperation("记录登录日志")
    @PostMapping
    public ResultData addLoginInfo(@RequestBody LoginInfoPo loginInfoPo){
        try {
            loginInfoService.addLoginInfo(loginInfoPo);
            return new ResultData("0000","操作成功");
        } catch (RuntimeException e ) {
            logger.error(e.getMessage(),e);
            return new ResultData("9999","操作失败");
        }
    }
}
