package bj.mapper;

import bj.po.SysLogPo;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

/**
 * @author eason
 */
@Repository
public interface SysLogMapper {

    int saveLog( SysLogPo sysLogPo);
}
