package bj.mapper;

import bj.po.User;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Map;

/**
 * @author eason
 */
@Repository
public interface UserMapper {

    List<User> getUser();

    List<User> getUserByPageInfo(Map<String, Object> map);

    void insertUser(User user);

    User getUserById(String id);

    User selectUserByUserName(String userName);

    void test(@Param("list") List<Object> dataList );

    void test1(@Param("list") List<Object> dataList );
}
