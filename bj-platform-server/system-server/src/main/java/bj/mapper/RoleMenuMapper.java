package bj.mapper;

import bj.po.Menu;
import bj.po.Role;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * @author eason
 */
@Repository
public interface RoleMenuMapper {

    List<String> getMenuByUserId(String userId);
}
