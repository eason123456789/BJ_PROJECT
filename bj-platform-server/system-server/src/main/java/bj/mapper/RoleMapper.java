package bj.mapper;

import bj.po.Role;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * @author eason
 */
@Repository
public interface RoleMapper {

    List<Role> getRole();
}
