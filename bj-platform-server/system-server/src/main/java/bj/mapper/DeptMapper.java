package bj.mapper;

import bj.po.Dept;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Map;

/**
 * @author eason
 */
@Repository
public interface DeptMapper {

    List<Map<String, Object>> getDeptTree();

}
