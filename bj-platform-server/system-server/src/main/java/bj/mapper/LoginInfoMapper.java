package bj.mapper;

import bj.po.LoginInfoPo;
import org.springframework.stereotype.Repository;

/**
 * @author eason
 */
@Repository
public interface LoginInfoMapper {

    void addLoginInfo(LoginInfoPo loginInfoPo);
}
