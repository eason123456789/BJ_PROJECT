//package bj.config;
//
//
//import bj.utils.AesUtil;
//import com.alibaba.fastjson2.JSON;
//import lombok.SneakyThrows;
//import lombok.extern.slf4j.Slf4j;
//import org.springframework.core.MethodParameter;
//import org.springframework.http.HttpHeaders;
//import org.springframework.http.HttpInputMessage;
//import org.springframework.http.converter.HttpMessageConverter;
//import org.springframework.util.StreamUtils;
//import org.springframework.web.bind.annotation.ControllerAdvice;
//import org.springframework.web.servlet.mvc.method.annotation.RequestBodyAdviceAdapter;
//
//import java.io.ByteArrayInputStream;
//import java.io.IOException;
//import java.io.InputStream;
//import java.lang.reflect.Type;
//import java.nio.charset.Charset;
//import java.util.List;
//
///**
// * @ClassName SecurityRequestAdvice
// * @Description
// * @Author ctjsoft-pt
// * @Date 2022/10/17 9:21
// * @Versin 1.0
// **/
//
//@ControllerAdvice
//@Slf4j
////@Order(Ordered.HIGHEST_PRECEDENCE)
//public class SecurityRequestAdvice extends RequestBodyAdviceAdapter {
//
//    private Boolean isDecrypt = Boolean.TRUE;
//
//    @Override
//    public boolean supports(MethodParameter methodParameter, Type type, Class<? extends HttpMessageConverter<?>> aClass) {
//        return true;
//    }
//
//
//    @SneakyThrows
//    @Override
//    public HttpInputMessage beforeBodyRead(HttpInputMessage inputMessage, MethodParameter parameter, Type targetType, Class<? extends HttpMessageConverter<?>> converterType) throws IOException {
//        String httpBody;
//        if (Boolean.TRUE.equals(isDecrypt)) {
//            httpBody = decryptBody(inputMessage);
//        } else {
//            httpBody = StreamUtils.copyToString(inputMessage.getBody(), Charset.defaultCharset());
//        }
//        return new SecretHttpMessage(new ByteArrayInputStream(httpBody.getBytes()), inputMessage.getHeaders());
//    }
//
//
//    /**
//     * 解密消息体
//     *
//     * @param inputMessage 消息体
//     * @return 明文
//     */
//    private String decryptBody(HttpInputMessage inputMessage) throws Exception {
//        InputStream bodyIS = inputMessage.getBody();
//        String bodyStr = StreamUtils.copyToString(bodyIS, Charset.defaultCharset());
//        String aesDecrypt = AesUtil.aesDecrypt(bodyStr);
//        List list = JSON.parseObject(aesDecrypt, List.class);
//        list.parallelStream().forEach(s -> log.info(list.toString()));
//        return aesDecrypt;
//    }
//
//    class SecretHttpMessage implements HttpInputMessage {
//
//        private InputStream body;
//        private HttpHeaders header;
//
//        SecretHttpMessage(InputStream body, HttpHeaders header) {
//            this.body = body;
//            this.header = header;
//        }
//
//        @Override
//        public InputStream getBody() throws IOException {
//            return this.body;
//        }
//
//        @Override
//        public HttpHeaders getHeaders() {
//            return this.header;
//        }
//    }
//}
