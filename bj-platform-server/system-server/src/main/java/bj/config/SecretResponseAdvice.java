//package bj.config;
//
//import bj.utils.AesUtil;
//import com.alibaba.fastjson2.JSON;
//import lombok.SneakyThrows;
//import lombok.extern.slf4j.Slf4j;
//import org.springframework.core.MethodParameter;
//import org.springframework.http.MediaType;
//import org.springframework.http.server.ServerHttpRequest;
//import org.springframework.http.server.ServerHttpResponse;
//import org.springframework.web.bind.annotation.ControllerAdvice;
//import org.springframework.web.servlet.mvc.method.annotation.ResponseBodyAdvice;
//
///**
// * @ClassName SecretResponseAdvice
// * @Description
// * @Author ctjsoft-pt
// * @Date 2022/10/17 10:10
// * @Versin 1.0
// **/
//@Slf4j
//@ControllerAdvice
//public class SecretResponseAdvice implements ResponseBodyAdvice {
//
//    private Boolean isCrypt = Boolean.TRUE;
//
//    @Override
//    public boolean supports(MethodParameter methodParameter, Class aClass) {
//        return true;
//    }
//
//    @SneakyThrows
//    @Override
//    public Object beforeBodyWrite(Object o, MethodParameter methodParameter, MediaType mediaType, Class aClass, ServerHttpRequest serverHttpRequest, ServerHttpResponse serverHttpResponse) {
//        String path = serverHttpRequest.getURI().getPath();
//
//        if (isCrypt) {
////            String encrypt = AesUtil.encrypt(JSON.toJSONString(o));
//            String encrypt = "AesUtil.encrypt(JSON.toJSONString(o));";
//            return encrypt;
//        }
//        return o;
//    }
//
//}
