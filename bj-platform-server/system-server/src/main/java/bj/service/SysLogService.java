package bj.service;

import bj.po.SysLogPo;

/**
 * @author eason
 */
public interface SysLogService {

    int saveLog( SysLogPo sysLogPo);
}
