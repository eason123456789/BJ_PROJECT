package bj.service;

import java.util.Set;

/**
 * @author eason
 */
public interface UserRoleService {

    public Set<Integer> getRoleByUserId(String userId);
}
