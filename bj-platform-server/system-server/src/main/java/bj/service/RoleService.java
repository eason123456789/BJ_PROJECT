package bj.service;

import bj.po.Role;

import java.util.List;

public interface RoleService {

    List<Role> getRole();
}
