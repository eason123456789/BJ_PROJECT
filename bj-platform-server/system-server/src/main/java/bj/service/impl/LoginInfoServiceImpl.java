package bj.service.impl;

import bj.mapper.LoginInfoMapper;
import bj.po.LoginInfoPo;
import bj.service.LoginInfoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * @author eason
 */
@Service
public class LoginInfoServiceImpl implements LoginInfoService {
    @Autowired
    private LoginInfoMapper loginInfoMapper;

    @Override
    public void addLoginInfo(LoginInfoPo loginInfoPo){
        loginInfoMapper.addLoginInfo(loginInfoPo);
    }
}
