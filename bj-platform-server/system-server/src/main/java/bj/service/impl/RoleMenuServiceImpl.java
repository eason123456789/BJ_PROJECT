package bj.service.impl;

import bj.mapper.RoleMenuMapper;
import bj.service.RoleMenuService;
import cn.hutool.core.lang.hash.Hash;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * @author eason
 */
@Service
public class RoleMenuServiceImpl implements RoleMenuService {

    @Autowired
    private RoleMenuMapper roleMenuMapper;

    @Override
    public Set<String> getMenuByUserId(String userId){
        List<String> menuByUserId = roleMenuMapper.getMenuByUserId(userId);
        Set<String> permissions= new HashSet<>();
        permissions.addAll(menuByUserId);
        return permissions;
    }
}
