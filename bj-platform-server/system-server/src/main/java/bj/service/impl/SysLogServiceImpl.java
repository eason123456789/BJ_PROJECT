package bj.service.impl;

import bj.mapper.SysLogMapper;
import bj.po.SysLogPo;
import bj.service.SysLogService;
import cn.hutool.core.lang.UUID;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * @author eason
 */
@Service
public class SysLogServiceImpl implements SysLogService {

    @Autowired
    private SysLogMapper sysLogMapper;

    @Override
    public int saveLog(SysLogPo sysLogPo) {
        sysLogPo.setDelFlag(2L);
       return sysLogMapper.saveLog(sysLogPo);
    }
}
