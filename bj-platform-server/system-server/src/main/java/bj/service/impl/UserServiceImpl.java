package bj.service.impl;

import bj.mapper.UserMapper;
import bj.po.User;
import bj.service.UserService;
import cn.hutool.core.util.IdUtil;
import cn.hutool.core.util.StrUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.*;
import java.util.stream.Collectors;

@Service
public class UserServiceImpl implements UserService {
    @Autowired
    private UserMapper userMapper;

    /**
     * @Description:查询用户列表
     * @Author: Eason
     * @Date: 2022/7/19 下午4:55
     * @return: java.util.List<bj.po.User>
     **/
    @Override
    public List<User> getUser() {
        return userMapper.getUser();
    }

    /**
     * @Description:新建用户
     * @Author: Eason
     * @Date: 2022/7/19 下午4:55
     * @param: user
     * @return: void
     **/
    @Override
    public void insertUser(User user){
        user.setId(IdUtil.randomUUID());
        BCryptPasswordEncoder bCryptPasswordEncoder=new BCryptPasswordEncoder();
        String password = bCryptPasswordEncoder.encode(user.getPassword());
        user.setPassword(password);
        userMapper.insertUser(user);
    }

    /**
     * @Description:查询用户(分页)
     * @Author: Eason
     * @Date: 2022/7/19 下午4:55
     * @param: page
     * @param: pageSize
     * @param: params
     * @return: java.util.List<bj.po.User>
     **/
    @Override
    public List<User> getUserByPageInfo(int page,int pageSize,Map<String, Object> params){
        Map<String, Object> map=new HashMap<>();
        int startRow=(page-1)*pageSize;
        int endRow=page*pageSize;
        map.put("startRow",startRow);
        map.put("endRow",endRow);
        List<User> users = userMapper.getUserByPageInfo(map);
        return users;
    }

    /**
     * @Description:查询用户(根据用户ID)
     * @Author: Eason
     * @Date: 2022/7/19 下午4:58
     * @param: id
     * @return: bj.po.User
     **/
    @Override
    public User getUserById(String id){
        if (!StrUtil.isEmpty(id)){
            User user = userMapper.getUserById(id);
            return user;
        }
        return null;
    }

    /**
     * @Description:查询用户(根据用户名)
     * @Author: Eason
     * @Date: 2022/8/9 下午2:04
     * @param: userName
     * @return: bj.po.User
     **/
    @Override
    public User selectUserByUserName(String userName) {
        return userMapper.selectUserByUserName(userName);
    }

    @Override
    public List<Object> test(List<Object> list){
        List<Object> collect = list.stream().sorted().collect(Collectors.toList());
        userMapper.test(list);
        userMapper.test1(collect);
        return collect;
    }
}
