package bj.service.impl;

import bj.mapper.RoleMapper;
import bj.po.Role;
import bj.service.RoleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class RoleServiceImpl implements RoleService {
    @Autowired
    private RoleMapper roleMapper;

    @Override
    public List<Role> getRole() {
        List<Role> roleList = roleMapper.getRole();
        return roleList;
    }
}
