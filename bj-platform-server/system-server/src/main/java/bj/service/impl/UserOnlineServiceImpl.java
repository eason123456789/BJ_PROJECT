package bj.service.impl;

import bj.po.LoginUser;
import bj.po.UserOnlinePo;
import bj.service.UserOnlineService;
import org.springframework.stereotype.Service;

/**
 * @author eason
 */
@Service
public class UserOnlineServiceImpl implements UserOnlineService {

    @Override
    public UserOnlinePo getOnlineUser(LoginUser loginUser){
        if (loginUser==null){
            return null;
        }
        UserOnlinePo userOnlinePo=new UserOnlinePo();
        userOnlinePo.setTokenId(loginUser.getToken());
        userOnlinePo.setLoginTime(loginUser.getLoginTime());
        userOnlinePo.setUserName(loginUser.getUsername());
        userOnlinePo.setIpaddr(loginUser.getIpaddr());
        return userOnlinePo;
    }
}