package bj.service.impl;

import bj.mapper.UserRoleMapper;
import bj.po.Role;
import bj.service.UserRoleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * @author eason
 */
@Service
public class UserRoleServiceImpl implements UserRoleService {

    @Autowired
    private UserRoleMapper userRoleMapper;


    @Override
    public Set<Integer> getRoleByUserId(String userId){
        if (userId!=null && userId!=""){
            List<Role> roleList = userRoleMapper.getRoleByUserId(userId);
            List<Integer> collect = roleList.stream().map(map -> map.getIs_admin()).collect(Collectors.toList());
            Set<Integer> permsSet = new HashSet<>();
            permsSet.addAll(collect);
            return permsSet;
        }
        return null;
    }
}
