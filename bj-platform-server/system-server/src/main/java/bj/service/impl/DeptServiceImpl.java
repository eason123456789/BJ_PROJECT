package bj.service.impl;

import bj.mapper.DeptMapper;
import bj.service.DeptService;
import bj.utils.ListTreeUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * @author eason
 */
@Service
public class DeptServiceImpl implements DeptService {
    @Autowired
    private DeptMapper deptMapper;

    /**
     * @Description:查询部门树🌲
     * @Author: Eason
     * @Date: 2022/7/21 下午4:37
     * @return: java.util.List<java.util.Map<java.lang.String,java.lang.Object>>
     **/
    @Override
    public List<Map<String, Object>> getDeptTree() {

        List<Map<String, Object>> deptTree=new ArrayList<>();

        List<Map<String, Object>> deptList = deptMapper.getDeptTree();


        for (Map<String, Object> map : deptList) {
            if ( "0".equals(map.get("parent_id").toString())) {
                ListTreeUtil.TreeList(deptList,map,map.get("dept_id").toString(),"dept_id","parent_id");
                deptTree.add(map);
            }
        }
        return deptTree;
    }


}
