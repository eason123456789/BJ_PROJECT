package bj.service;

import bj.po.LoginInfoPo;

/**
 * @author eason
 */
public interface LoginInfoService {

    void addLoginInfo(LoginInfoPo loginInfoPo);
}
