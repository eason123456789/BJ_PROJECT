package bj.service;

import java.util.Set;

/**
 * @author eason
 */
public interface RoleMenuService {

    public Set<String> getMenuByUserId(String userId);
}
