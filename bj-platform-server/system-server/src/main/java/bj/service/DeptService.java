package bj.service;

import java.util.List;
import java.util.Map;

/**
 * @author eason
 */
public interface DeptService {

    List<Map<String, Object>> getDeptTree();
}
