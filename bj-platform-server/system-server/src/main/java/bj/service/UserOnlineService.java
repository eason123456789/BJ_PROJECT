package bj.service;

import bj.po.LoginUser;
import bj.po.UserOnlinePo;

/**
 * @author eason
 */
public interface UserOnlineService {

     UserOnlinePo getOnlineUser(LoginUser loginUser);
}
