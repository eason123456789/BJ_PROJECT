package bj.service;


import bj.po.User;
import com.github.pagehelper.PageInfo;

import java.util.List;
import java.util.Map;
import java.util.TreeSet;

public interface UserService {

    List<User> getUser();

    void insertUser(User user);

    List<User> getUserByPageInfo(int page,int pageSize,Map<String, Object> params);

    User getUserById(String id);

    User selectUserByUserName(String userName);

    List<Object> test(List<Object> list);
}
