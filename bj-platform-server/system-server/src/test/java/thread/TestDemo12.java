package thread;
/**
 * @Description:线程的优先级
 * @Author: Eason
 * @Date: 2023/2/21
 * @param: null
 * @return: null
 **/
public class TestDemo12 {
    public static void main(String[] args) {
        Thread thread1 = new Thread(() -> {
            System.out.println(Thread.currentThread().getName()+"开始执行");
        });
        Thread thread2 = new Thread(() -> {
            System.out.println(Thread.currentThread().getName()+"开始执行");
        });

        //设置线程的优先级
        thread1.setPriority(Thread.MAX_PRIORITY);
        thread2.setPriority(Thread.NORM_PRIORITY);
        thread1.start();
        thread2.start();
    }
}
