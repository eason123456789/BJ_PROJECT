package thread;

public class ThreadDemo03 implements Runnable{

    private static Integer count=100;

    Object object=new Object();

    @Override
    public void run() {
        while (count>1){
          cal();
        }
    }

    private synchronized void cal(){
        try {
            Thread.sleep(10);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
            count--;
            System.out.println(Thread.currentThread().getName() + "====" + count);
    }

    public static void main(String[] args) {
        ThreadDemo03 threadDemo03=new ThreadDemo03();
        Thread thread1 = new Thread(threadDemo03);
        Thread thread2 = new Thread(threadDemo03);
        thread1.start();
        thread2.start();
    }
}
