package thread;
/**
 * @Description: wait/notify/简单的用法
 * @Author: Eason 
 * @Date: 2023/2/20 上午10:42
 * @param: null 
 * @return: null
 **/
public class ThreadDemo10 extends Thread{

    @Override
    public void run() {
        try {
            synchronized (this) {
                System.out.println(Thread.currentThread().getName() + ">>当前线程阻塞，同时释放锁!<<");
                this.wait();
            }
            System.out.println(">>run()<<");
        } catch (InterruptedException e) {

        }
    }

    public static void main(String[] args) {
        ThreadDemo10 thread = new ThreadDemo10();
        thread.start();
        try {
            Thread.sleep(3000);
        } catch (Exception e) {

        }
        synchronized (thread) {
            // 唤醒正在阻塞的线程
            thread.notify();
        }

    }

}
