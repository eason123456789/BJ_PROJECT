package thread;

public class ThreadDemo07 {
    private final Object object = new Object();

    public static void main(String[] args) throws InterruptedException {
        ThreadDemo07 threadDemo07 = new ThreadDemo07();
        Thread thread = threadDemo07.print();
        thread.start();
        try {
            Thread.sleep(3000);
            thread.interrupt();
        } catch (Exception e) {

        }

    }

    public Thread print() {
        Thread thread = new Thread(() -> {
            synchronized (object) {
                System.out.println("1");
                try {
                    object.wait(0);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                System.out.println("2");
            }
        });
        return thread;
    }

}
