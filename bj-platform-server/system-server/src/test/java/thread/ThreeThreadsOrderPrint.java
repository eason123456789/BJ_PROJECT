package thread;

import org.junit.jupiter.api.Test;

import java.util.concurrent.CountDownLatch;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.ReadWriteLock;
import java.util.concurrent.locks.ReentrantLock;

public class ThreeThreadsOrderPrint {
    @Test
    public void testUseCountDownLatch() {
        ExecutorService executorService = Executors.newCachedThreadPool();
        CountDownLatch aLatch = new CountDownLatch(1);
        CountDownLatch bLatch = new CountDownLatch(1);
        CountDownLatch cLatch = new CountDownLatch(1);

        executorService.submit(() -> {
            try {
                aLatch.await();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            for (int i = 0; i <10 ; i++) {
                System.out.println("A->"+i);
            }
            bLatch.countDown();
        });

        executorService.submit(() -> {
            try {
                bLatch.await();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }

            for (int i = 0; i <10 ; i++) {
                System.out.println("B->"+i);
            }

            cLatch.countDown();
        });

        executorService.submit(() -> {
            try {
                cLatch.await();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }

            for (int i = 0; i <10 ; i++) {
                System.out.println("C->"+i);
            }
        });

        aLatch.countDown();
    }


    private static final ReentrantLock lock = new ReentrantLock();
    private static final Condition c1 = lock.newCondition();
    private static final Condition c2 = lock.newCondition();
    private static final Condition c3 = lock.newCondition();
    public static void main(String[] args) {

        new Thread(() -> {
            try {
                lock.lock();

                for (int i = 0; i < 10; i++) {
                    System.out.println("A - " + i);
                    c2.signal();
                    c1.await();
                }

            } catch (InterruptedException e) {
                e.printStackTrace();
            } finally {
                lock.unlock();
            }
        }).start();

        new Thread(() -> {
            try {
                lock.lock();

                for (int i = 0; i < 10; i++) {
                    System.out.println("B - " + i);
                    c3.signal();
                    c2.await();
                }

            } catch (InterruptedException e) {
                e.printStackTrace();
            } finally {
                lock.unlock();
            }
        }).start();

        new Thread(() -> {
            try {
                lock.lock();

                for (int i = 0; i < 10; i++) {
                    System.out.println("C - " + i);
                    c1.signal();
                    c3.await();
                }

            } catch (InterruptedException e) {
                e.printStackTrace();
            } finally {
                lock.unlock();
            }
        }).start();

    }

    @Test
    public void testUseCountDownLatch1() {
        ExecutorService executorService1 = Executors.newCachedThreadPool();
        CountDownLatch countDownLatch = new CountDownLatch(1);
        executorService1.submit(() -> {
            try {
                countDownLatch.await();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }

            System.out.println("A->" + System.currentTimeMillis());
        });

        executorService1.submit(() -> {
            try {
                countDownLatch.await();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }

            System.out.println("B->" + System.currentTimeMillis());
        });

        executorService1.submit(() -> {
            try {
                countDownLatch.await();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            System.out.println("C->" + System.currentTimeMillis());
        });
        countDownLatch.countDown();
    }


    @Test
    public void testUseCountDownLatch2() {
        ExecutorService executorService1 = Executors.newCachedThreadPool();
        executorService1.submit(() -> {
            System.out.println("A->" + System.currentTimeMillis());
        });

        executorService1.submit(() -> {
            System.out.println("B->" + System.currentTimeMillis());
        });

        executorService1.submit(() -> {
            System.out.println("C->" + System.currentTimeMillis());
        });

    }

}

