package thread;

import lombok.SneakyThrows;
/**
 * @Description: 模拟生产者与消费者线程安全模式
 * @Author: Eason
 * @Date: 2022/10/22 下午10:31
 * @param: null
 * @return: null
 **/
public class ThreadDemo05 {

    class Res {
        /**
         * 姓名
         */
        private String userName;
        /**
         * 性别
         */
        private char sex;

        /**
         **标志
         **/
        private boolean flag=false;
    }

    class InputThread extends Thread {
        private final Res res;

        public InputThread(Res res) {
            this.res = res;
        }

        @SneakyThrows
        @Override
        public void run() {
                int count = 0;
                while (true) {
                    synchronized (res) {
                    if (res.flag){
                        res.wait();
                    }
                    if (count == 0) {
                        this.res.userName = "张三";
                        this.res.sex = '女';
                    } else {
                        this.res.userName = "李四";
                        this.res.sex = '男';
                    }
                    count = (count + 1) % 2;
                    res.flag=true;
                    res.notify();
                }
            }
        }
    }

    class OutThread extends Thread {
        private final Res res;

        public OutThread(Res res) {
            this.res = res;
        }


        @SneakyThrows
        @Override
        public void run() {
                while (true) {
                    synchronized (res) {
                        if (!res.flag){
                            res.wait();
                        }
                        System.out.println(res.userName + "," + res.sex);
                        res.flag=false;
                        res.notify();
                }
            }
        }
    }


    public static void main(String[] args) {
        new ThreadDemo05().print();
    }

    public void print() {
        Res res = new Res();
        InputThread inputThread = new InputThread(res);
        OutThread outThread = new OutThread(res);
        inputThread.start();
        outThread.start();
    }

}
