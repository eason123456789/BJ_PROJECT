package thread;

import java.util.concurrent.atomic.AtomicBoolean;

public class ThreadDemo14 extends Thread{


    private volatile  boolean flag=true;

    public void setFlag(boolean flag) {
        this.flag = flag;
    }

    @Override
    public void run() {
        while (flag){
            System.out.println("Running->");
            flag=false;
        }
    }

    public static void main(String[] args) {
        ThreadDemo14 threadDemo14=new ThreadDemo14();
        threadDemo14.start();
    }
}
