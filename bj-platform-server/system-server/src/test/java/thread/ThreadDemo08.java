package thread;
/**
 * @Description: 模拟守护线程和用户线程
 * @Author: Eason
 * @Date: 2022/10/24 上午10:49
 * @param: null
 * @return: null
 **/
public class ThreadDemo08 {
    public static void main(String[] args) {
        System.out.println("我是主线程");
        Thread thread=new Thread(()->{
            while (true) {
                try {
                    Thread.sleep(1000);
                    System.out.println(Thread.currentThread().getName() + "=====我是子线程");
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        });
        //设置为守护线程，setDaemon:false为用户线程、true为用户线程
        thread.setDaemon(true);
        thread.start();
    }
}
