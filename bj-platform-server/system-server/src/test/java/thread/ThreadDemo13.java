package thread;

/**
 * @Description:打断正在阻塞的线程
 * @Author: Eason
 * @Date: 2023/2/21 下午3:12
 * @param: null
 * @return: null
 **/
public class ThreadDemo13 {

    public static void main(String[] args) {
        Thread thread = new Thread(() -> {
            try {
                Thread.sleep(5000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }

        });
        thread.start();
        try {
            Thread.sleep(1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        System.out.println("打断子线程");
        thread.interrupt();
        System.out.println("获取打断标记："+thread.isInterrupted());
    }
}
