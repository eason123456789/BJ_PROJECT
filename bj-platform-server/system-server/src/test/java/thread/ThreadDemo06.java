package thread;
/**
 * @Description: 研究线程join原理
 * @Author: Eason
 * @Date: 2022/10/23 下午9:24
 * @param: null
 * @return: null
 **/
public class ThreadDemo06 {

    public static void main(String[] args) {
        Thread t1 = new Thread(() -> {
            System.out.println(Thread.currentThread().getName() + "====t1");
        });
        Thread t2 = new Thread(() -> {
            try {
                t1.join();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            System.out.println(Thread.currentThread().getName() + "====t2");
        });
        Thread t3 = new Thread(() -> {
            try {
                t2.join();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            System.out.println(Thread.currentThread().getName() + "====t3");
        });
        t1.start();
        t2.start();
        t3.start();
    }
}
