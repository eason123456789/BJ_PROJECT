package thread;

/**
 * @Description: 模拟线程死锁情况
 * @Author: Eason
 * @Date: 2022/10/22 下午10:33
 * @param: null
 * @return: null
 **/
public class ThreadDemo04 implements Runnable{
    private int count=1;
    private final String lock="lock";

    @Override
    public void run() {
        while (true){
            count++;
            // 线程1需要获取 lock 在获取 a方法this锁
            // 线程2需要获取this 锁在 获取B方法lock锁
            if (count%2==0){
                synchronized (lock){
                    a();
                }
            }else {
                synchronized (this){
                   b();
                }
            }
        }
    }

    public synchronized void a(){
        System.out.println(Thread.currentThread().getName()+"===a方法");
    }

    public void b(){
        synchronized (lock) {
            System.out.println(Thread.currentThread().getName() + "===b方法");
        }
    }

    public static void main(String[] args) {
        ThreadDemo04 threadDemo04=new ThreadDemo04();
        Thread thread1=new Thread(threadDemo04);
        Thread thread2=new Thread(threadDemo04);
        thread1.start();
        thread2.start();

    }
}
