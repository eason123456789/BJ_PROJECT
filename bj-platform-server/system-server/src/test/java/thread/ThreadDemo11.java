package thread;

public class ThreadDemo11 extends Thread{
    private final String name;

    public ThreadDemo11(String name) {
        this.name = name;
    }

    @Override
    public void run() {
        for (int i = 0; i <50 ; i++) {
            if (i == 30) {
                System.out.println(Thread.currentThread().getName()+",释放cpu执行权");
                yield();
            }
            System.out.println(Thread.currentThread().getName()+","+i);
        }
    }

    public static void main(String[] args) {
        new ThreadDemo11("test01").start();
        new ThreadDemo11("test02").start();
    }

}
