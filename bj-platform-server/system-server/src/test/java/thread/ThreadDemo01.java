package thread;

public class ThreadDemo01 extends Thread {
    @Override
    public void run() {
        System.out.println(Thread.currentThread().getName()+"=====121212121");
    }

    public static void main(String[] args) throws InterruptedException {
        Thread.sleep(2000);
        Thread.yield();
        new ThreadDemo01().start();
    }
}
