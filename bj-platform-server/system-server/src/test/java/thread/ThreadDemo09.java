package thread;

import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

public class ThreadDemo09 implements Runnable{

    private static Integer count=100;
    private final Lock lock=new ReentrantLock();

    @Override
    public void run() {
        while (count>1){
            cal();
        }
    }

    private  void cal(){
        try {
            Thread.sleep(10);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        count--;
        System.out.println(Thread.currentThread().getName() + "====" + count);
    }

    public static void main(String[] args) {
        ThreadDemo09 ThreadDemo09=new ThreadDemo09();
        Thread thread1 = new Thread(ThreadDemo09);
        Thread thread2 = new Thread(ThreadDemo09);
        thread1.start();
        thread2.start();
    }


}
