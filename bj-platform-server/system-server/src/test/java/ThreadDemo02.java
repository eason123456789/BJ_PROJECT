import bj.po.User;

import java.util.ArrayList;
import java.util.List;


public class ThreadDemo02 implements Runnable{
    @Override
    public void run() {
        System.out.println(Thread.currentThread().getName()+",12121212212");
    }

    public static void main(String[] args) {
        List<Object> list=new ArrayList<>();
        list.toArray();
        //new Thread(new ThreadDemo02()).start();
        new Thread(new Runnable() {
            @Override
            public void run() {
                System.out.println(Thread.currentThread().getName()+",wqwqwqwqwq");
            }
        }).start();

        new Thread(()->{
            System.out.println(Thread.currentThread().getName()+",sdsdsds");
        }).start();

        new Thread(()->{
            System.out.println("sasasasassaassaa");
        });
    }
}
