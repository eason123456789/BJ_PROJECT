package mq;

import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;
import lombok.SneakyThrows;

public class producer {


    @SneakyThrows
    public static void main(String[] args) {
        Connection connection = mqConnection.getConnection();
        Channel channel = connection.createChannel();

        String msg="测试MQ";
        System.out.println("msg:"+msg);
        for (int i = 0; i <20 ; i++) {
            channel.basicPublish("","/test",null,msg.getBytes());
        }
        channel.close();
        connection.close();
    }
}
