package mq;

import com.rabbitmq.client.*;
import lombok.SneakyThrows;

import java.io.IOException;
import java.nio.charset.StandardCharsets;

public class consumer {

    @SneakyThrows
    public static void main(String[] args) {
        Connection connection = mqConnection.getConnection();

        // 2.设置通道
        Channel channel = connection.createChannel();
        DefaultConsumer defaultConsumer = new DefaultConsumer(channel) {
        @Override
        public void handleDelivery(String consumerTag, Envelope envelope, AMQP.BasicProperties properties, byte[] body) throws IOException {
            System.out.println("111");
            String msg = new String(body, StandardCharsets.UTF_8);
            System.out.println("消费者获取消息:" + msg);
        }
    };
        // 3.监听队列
        //false采用手动应答
        channel.basicConsume("/test", false, defaultConsumer);
 }
}
