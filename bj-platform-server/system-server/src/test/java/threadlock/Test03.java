package threadlock;

public class Test03 extends Thread{
    private int count = 1;
    private final String lock = "lock";


    @Override
    public void run() {
        while (true) {
            count++;
            if (count % 2 == 0) {
                // 线程1需要获取 lock 在获取 a方法this锁
                // 线程2需要获取this 锁在 获取B方法lock锁
                synchronized (lock) {
                    a();
                }
            } else {
                synchronized (this) {
                    b();
                }
            }
        }

    }

    public static void main(String[] args) {
        Test03 test03 = new Test03();
        Thread thread1 = new Thread(test03);
        Thread thread2 = new Thread(test03);
        thread1.start();
        thread2.start();


    }

    public synchronized void a() {
        System.out.println(Thread.currentThread().getName() + ",a方法...");
    }

    public void b() {
        synchronized (lock) {
            System.out.println(Thread.currentThread().getName() + ",b方法...");
        }
    }

}
