package threadlock;

public class Test02 extends Thread {
    private static Integer count = 100;

    @Override
    public void run() {
        while (count > 1) {
            cal();
        }
    }

    private static  void cal() {
        //synchronized关键字修饰静态方法
        synchronized (Test02.class){
            try {
                int i = 1 / 0;
                Thread.sleep(20);
            } catch (Exception e) {

            }
            count--;
            System.out.println(Thread.currentThread().getName() + "," + count);
        }
    }


    public static void main(String[] args) {
        Test02 threadCount = new Test02();
        Thread thread1 = new Thread(threadCount);
        Thread thread2 = new Thread(threadCount);
        thread1.start();
        thread2.start();
    }
}
