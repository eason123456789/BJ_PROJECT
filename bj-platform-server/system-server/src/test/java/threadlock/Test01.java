package threadlock;

/**
 * @Description:多线程安全问题
 * @Author: Eason
 * @Date: 2023/2/18 下午8:59
 * @param: null
 * @return: null
 **/
public class Test01 extends Thread {
    private static Integer count = 100;

    @Override
    public void run() {
        while (count > 1) {
            cal();
        }
    }

    private  void cal() {
        try {
            Thread.sleep(200);
        } catch (Exception e) {

        }
        count--;
        System.out.println(Thread.currentThread().getName() + "," + count);
    }


    public static void main(String[] args) {
        Test01 threadCount = new Test01();
        Thread thread1 = new Thread(threadCount);
        Thread thread2 = new Thread(threadCount);
        thread1.start();
        thread2.start();
    }

}
